-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 06, 2018 at 06:25 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `RailwayReservation`
--

-- --------------------------------------------------------

--
-- Table structure for table `Authentication`
--

CREATE TABLE `Authentication` (
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` char(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Carriage`
--

CREATE TABLE `Carriage` (
  `id` int(11) NOT NULL,
  `no` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `trainId` int(11) NOT NULL,
  `totalSeats` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Carriage`
--

INSERT INTO `Carriage` (`id`, `no`, `type`, `trainId`, `totalSeats`) VALUES
(45, 1, 1, 1, 60),
(46, 2, 1, 1, 60),
(47, 3, 1, 1, 60),
(48, 4, 1, 1, 60),
(49, 5, 1, 1, 60),
(50, 6, 2, 1, 40),
(51, 7, 2, 1, 40),
(52, 8, 2, 1, 40),
(53, 9, 2, 1, 40),
(54, 10, 2, 1, 40),
(55, 11, 2, 1, 40),
(56, 1, 1, 2, 60),
(57, 2, 1, 2, 60),
(58, 3, 1, 2, 60),
(59, 4, 1, 2, 60),
(60, 5, 1, 2, 60),
(61, 6, 2, 2, 40),
(62, 7, 2, 2, 40),
(63, 8, 2, 2, 40),
(64, 9, 2, 2, 40),
(65, 10, 2, 2, 40),
(66, 11, 2, 2, 40),
(67, 1, 1, 3, 60),
(68, 2, 1, 3, 60),
(69, 3, 1, 3, 60),
(70, 4, 2, 3, 40),
(71, 5, 2, 3, 40),
(72, 6, 2, 3, 40),
(73, 1, 1, 4, 60),
(74, 2, 1, 4, 60),
(75, 3, 1, 4, 60),
(76, 4, 2, 4, 40),
(77, 5, 2, 4, 40),
(78, 6, 2, 4, 40);

-- --------------------------------------------------------

--
-- Table structure for table `CarriageType`
--

CREATE TABLE `CarriageType` (
  `id` int(11) NOT NULL,
  `type` enum('ngồi mềm điều hoà','giường nằm điều hoà') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CarriageType`
--

INSERT INTO `CarriageType` (`id`, `type`) VALUES
(1, 'ngồi mềm điều hoà'),
(2, 'giường nằm điều hoà');

-- --------------------------------------------------------

--
-- Table structure for table `Cart`
--

CREATE TABLE `Cart` (
  `id` int(11) NOT NULL,
  `carriageId` int(11) NOT NULL,
  `seatNo` int(11) NOT NULL,
  `expiryDate` date NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `isForward` tinyint(1) NOT NULL,
  `startStation` int(11) NOT NULL,
  `endStation` int(11) NOT NULL,
  `transactionId` int(11) NOT NULL,
  `addOn` datetime NOT NULL,
  `expiryTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `id` int(11) NOT NULL,
  `firstName` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `phone` char(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Customer`
--

INSERT INTO `Customer` (`id`, `firstName`, `lastName`, `phone`, `email`, `dob`) VALUES
(1, 'Huy', 'Nguyen', NULL, 'qhquanghuy96@gmail.com', '2020-01-01'),
(2, 'Dummy', 'Test', NULL, 'dummyemail@somedomain.com', '1996-05-06'),
(5, 'test 2', 's', NULL, 'test2@gmail.com', '1998-03-01');

-- --------------------------------------------------------

--
-- Table structure for table `Passenger`
--

CREATE TABLE `Passenger` (
  `id` int(11) NOT NULL,
  `firstName` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` char(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('student','children','oldster','adult','baby') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Passenger`
--

INSERT INTO `Passenger` (`id`, `firstName`, `lastName`, `dob`, `email`, `phone`, `type`) VALUES
(1, 'Huy', 'Nguyen', '1996-03-23', NULL, NULL, 'student'),
(2, 'Test1', 'Nguyen', '1996-06-23', NULL, NULL, 'adult');

-- --------------------------------------------------------

--
-- Table structure for table `Price`
--

CREATE TABLE `Price` (
  `id` int(11) NOT NULL,
  `pricePerKm` decimal(9,4) NOT NULL,
  `type` int(11) NOT NULL,
  `lowerBound` int(11) NOT NULL,
  `upperBound` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Price`
--

INSERT INTO `Price` (`id`, `pricePerKm`, `type`, `lowerBound`, `upperBound`) VALUES
(117, '999.3610', 1, 0, 30),
(118, '980.3922', 1, 30, 60),
(119, '962.1300', 1, 60, 90),
(120, '944.5357', 1, 90, 120),
(121, '927.5733', 1, 120, 150),
(122, '911.2094', 1, 150, 180),
(123, '895.4129', 1, 180, 210),
(124, '880.1548', 1, 210, 240),
(125, '865.4079', 1, 240, 270),
(126, '851.1471', 1, 270, 300),
(127, '837.3486', 1, 300, 330),
(128, '823.9904', 1, 330, 360),
(129, '811.0517', 1, 360, 390),
(130, '798.5131', 1, 390, 420),
(131, '786.3562', 1, 420, 450),
(132, '774.5640', 1, 450, 480),
(133, '763.1202', 1, 480, 510),
(134, '752.0096', 1, 510, 540),
(135, '741.2179', 1, 540, 570),
(136, '730.7316', 1, 570, 600),
(137, '720.5378', 1, 600, 630),
(138, '710.6246', 1, 630, 660),
(139, '700.9804', 1, 660, 690),
(140, '691.5945', 1, 690, 720),
(141, '682.4566', 1, 720, 750),
(142, '673.5570', 1, 750, 780),
(143, '664.8866', 1, 780, 810),
(144, '656.4365', 1, 810, 840),
(145, '648.1985', 1, 840, 870),
(146, '640.1647', 1, 870, 900),
(147, '632.3277', 1, 900, 930),
(148, '624.6802', 1, 930, 960),
(149, '617.2154', 1, 960, 990),
(150, '609.9270', 1, 990, 1020),
(151, '602.8087', 1, 1020, 1050),
(152, '595.8546', 1, 1050, 1080),
(153, '589.0592', 1, 1080, 1110),
(154, '582.4169', 1, 1110, 1140),
(155, '575.9229', 1, 1140, 1170),
(156, '569.5720', 1, 1170, 1200),
(157, '563.3597', 1, 1200, 1230),
(158, '557.2814', 1, 1230, 1260),
(159, '551.3329', 1, 1260, 1290),
(160, '545.5100', 1, 1290, 1320),
(161, '539.8089', 1, 1320, 1350),
(162, '534.2257', 1, 1350, 1380),
(163, '528.7568', 1, 1380, 1410),
(164, '523.3987', 1, 1410, 1440),
(165, '518.1481', 1, 1440, 1470),
(166, '513.0019', 1, 1470, 1500),
(167, '507.9568', 1, 1500, 1530),
(168, '503.0100', 1, 1530, 1560),
(169, '498.1587', 1, 1560, 1590),
(170, '493.4000', 1, 1590, 1620),
(171, '488.7313', 1, 1620, 1650),
(172, '484.1502', 1, 1650, 1680),
(173, '479.6542', 1, 1680, 1710),
(174, '475.2409', 1, 1710, 1740),
(175, '1853.3605', 2, 0, 30),
(176, '1818.1818', 2, 30, 60),
(177, '1784.3137', 2, 60, 90),
(178, '1751.6843', 2, 90, 120),
(179, '1720.2268', 2, 120, 150),
(180, '1689.8793', 2, 150, 180),
(181, '1660.5839', 2, 180, 210),
(182, '1632.2870', 2, 210, 240),
(183, '1604.9383', 2, 240, 270),
(184, '1578.4909', 2, 270, 300),
(185, '1552.9010', 2, 300, 330),
(186, '1528.1276', 2, 330, 360),
(187, '1504.1322', 2, 360, 390),
(188, '1480.8788', 2, 390, 420),
(189, '1458.3333', 2, 420, 450),
(190, '1436.4641', 2, 450, 480),
(191, '1415.2411', 2, 480, 510),
(192, '1394.6360', 2, 510, 540),
(193, '1374.6224', 2, 540, 570),
(194, '1355.1750', 2, 570, 600),
(195, '1336.2702', 2, 600, 630),
(196, '1317.8856', 2, 630, 660),
(197, '1300.0000', 2, 660, 690),
(198, '1282.5934', 2, 690, 720),
(199, '1265.6467', 2, 720, 750),
(200, '1249.1421', 2, 750, 780),
(201, '1233.0623', 2, 780, 810),
(202, '1217.3913', 2, 810, 840),
(203, '1202.1136', 2, 840, 870),
(204, '1187.2146', 2, 870, 900),
(205, '1172.6804', 2, 900, 930),
(206, '1158.4978', 2, 930, 960),
(207, '1144.6541', 2, 960, 990),
(208, '1131.1374', 2, 990, 1020),
(209, '1117.9361', 2, 1020, 1050),
(210, '1105.0395', 2, 1050, 1080),
(211, '1092.4370', 2, 1080, 1110),
(212, '1080.1187', 2, 1110, 1140),
(213, '1068.0751', 2, 1140, 1170),
(214, '1056.2972', 2, 1170, 1200),
(215, '1044.7761', 2, 1200, 1230),
(216, '1033.5037', 2, 1230, 1260),
(217, '1022.4719', 2, 1260, 1290),
(218, '1011.6732', 2, 1290, 1320),
(219, '1001.1001', 2, 1320, 1350),
(220, '990.7458', 2, 1350, 1380),
(221, '980.6034', 2, 1380, 1410),
(222, '970.6667', 2, 1410, 1440),
(223, '960.9293', 2, 1440, 1470),
(224, '951.3853', 2, 1470, 1500),
(225, '942.0290', 2, 1500, 1530),
(226, '932.8549', 2, 1530, 1560),
(227, '923.8579', 2, 1560, 1590),
(228, '915.0327', 2, 1590, 1620),
(229, '906.3745', 2, 1620, 1650),
(230, '897.8786', 2, 1650, 1680),
(231, '889.5406', 2, 1680, 1710),
(232, '881.3559', 2, 1710, 1740);

-- --------------------------------------------------------

--
-- Table structure for table `Route`
--

CREATE TABLE `Route` (
  `id` int(11) NOT NULL,
  `name` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `distance` decimal(6,2) NOT NULL,
  `startStation` int(11) NOT NULL,
  `endStation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Route`
--

INSERT INTO `Route` (`id`, `name`, `distance`, `startStation`, `endStation`) VALUES
(1, 'Hà Nôi - Sài Gòn', '1726.00', 1, 30),
(2, 'Vinh - Hà Nội', '319.00', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Station`
--

CREATE TABLE `Station` (
  `id` int(11) NOT NULL,
  `name` char(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Station`
--

INSERT INTO `Station` (`id`, `name`) VALUES
(28, 'Biên Hòa'),
(25, 'Bình Thuận'),
(7, 'Chợ Sy'),
(29, 'Dĩ An'),
(20, 'Diêu Trì'),
(17, 'Đà Nẵng'),
(15, 'Đông Hà'),
(13, 'Đồng Hới'),
(11, 'Đồng Lê'),
(1, 'Hà Nội'),
(16, 'Huế'),
(10, 'Hương Phố'),
(27, 'Long Khánh'),
(6, 'Minh Khôi'),
(12, 'Minh Lễ'),
(14, 'Mỹ Đức'),
(3, 'Nam Định'),
(23, 'Nha Trang'),
(4, 'Ninh Bình'),
(22, 'Ninh Hoà'),
(2, 'Phủ Lý'),
(19, 'Quảng Ngãi'),
(30, 'Sài Gòn'),
(26, 'Suối Kiết'),
(18, 'Tam Kỳ'),
(5, 'Thanh Hoá'),
(24, 'Tháp Chàm'),
(21, 'Tuy Hoà'),
(8, 'Vinh'),
(9, 'Yên Trung');

-- --------------------------------------------------------

--
-- Table structure for table `Ticket`
--

CREATE TABLE `Ticket` (
  `id` int(11) NOT NULL,
  `carriageId` int(11) NOT NULL,
  `seatNo` int(11) NOT NULL,
  `expiryDate` date NOT NULL,
  `expiryTime` time NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `isForward` tinyint(1) NOT NULL,
  `startStation` int(11) NOT NULL,
  `endStation` int(11) NOT NULL,
  `transactionId` int(11) NOT NULL,
  `status` enum('unused','used','returned') COLLATE utf8_unicode_ci NOT NULL,
  `returnDate` datetime NOT NULL,
  `checkInDate` time DEFAULT NULL,
  `passengerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Ticket`
--

INSERT INTO `Ticket` (`id`, `carriageId`, `seatNo`, `expiryDate`, `expiryTime`, `price`, `isForward`, `startStation`, `endStation`, `transactionId`, `status`, `returnDate`, `checkInDate`, `passengerId`) VALUES
(1, 45, 10, '2018-05-05', '06:00:00', '820000.00', 1, 1, 30, 2, 'unused', '0000-00-00 00:00:00', NULL, 1),
(2, 45, 11, '2018-05-05', '06:00:00', '820000.00', 1, 1, 30, 3, 'unused', '0000-00-00 00:00:00', NULL, 1),
(3, 45, 12, '2018-05-05', '06:00:00', '820000.00', 1, 1, 30, 4, 'returned', '2018-05-04 09:00:00', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Train`
--

CREATE TABLE `Train` (
  `id` int(11) NOT NULL,
  `routeId` int(11) NOT NULL,
  `label` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `startTime` time NOT NULL,
  `isForward` tinyint(1) NOT NULL,
  `totalCarriages` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Train`
--

INSERT INTO `Train` (`id`, `routeId`, `label`, `startTime`, `isForward`, `totalCarriages`) VALUES
(1, 1, 'SE1', '06:00:00', 1, 11),
(2, 1, 'SE2', '06:00:00', 0, 11),
(3, 2, 'NA1', '22:45:00', 0, 6),
(4, 2, 'NA2', '22:45:00', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `TrainRouting`
--

CREATE TABLE `TrainRouting` (
  `id` int(11) NOT NULL,
  `trainId` int(11) NOT NULL,
  `stationId` int(11) NOT NULL,
  `arriveTime` time NOT NULL,
  `leaveTime` time NOT NULL,
  `distance` int(11) NOT NULL,
  `dayOffset` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TrainRouting`
--

INSERT INTO `TrainRouting` (`id`, `trainId`, `stationId`, `arriveTime`, `leaveTime`, `distance`, `dayOffset`) VALUES
(1, 1, 1, '06:00:00', '06:00:00', 0, 0),
(2, 1, 2, '07:03:00', '07:06:00', 56, 0),
(3, 1, 3, '07:42:00', '07:47:00', 87, 0),
(4, 1, 4, '08:19:00', '08:22:00', 115, 0),
(5, 1, 5, '09:28:00', '09:31:00', 175, 0),
(6, 1, 6, '09:52:00', '09:54:00', 197, 0),
(7, 1, 7, '11:18:00', '11:21:00', 279, 0),
(8, 1, 8, '12:01:00', '12:08:00', 319, 0),
(9, 1, 9, '12:32:00', '12:35:00', 340, 0),
(10, 1, 10, '13:39:00', '13:41:00', 387, 0),
(11, 1, 11, '14:42:00', '14:44:00', 436, 0),
(12, 1, 12, '15:33:00', '15:35:00', 482, 0),
(13, 1, 13, '16:19:00', '16:34:00', 522, 0),
(14, 1, 14, '17:07:00', '17:09:00', 551, 0),
(15, 1, 15, '18:29:00', '18:32:00', 622, 0),
(16, 1, 16, '19:45:00', '19:53:00', 688, 0),
(17, 1, 17, '22:31:00', '22:47:00', 791, 0),
(18, 1, 18, '00:05:00', '00:08:00', 865, 1),
(19, 1, 19, '01:16:00', '01:21:00', 928, 1),
(20, 1, 20, '04:08:00', '04:23:00', 1096, 1),
(21, 1, 21, '06:16:00', '06:22:00', 1198, 1),
(22, 1, 22, '07:52:00', '07:55:00', 1281, 1),
(23, 1, 23, '08:28:00', '08:36:00', 1315, 1),
(24, 1, 24, '10:07:00', '10:12:00', 1408, 1),
(25, 1, 25, '12:33:00', '12:38:00', 1551, 1),
(26, 1, 26, '13:38:00', '13:40:00', 1603, 1),
(27, 1, 27, '14:25:00', '14:28:00', 1649, 1),
(28, 1, 28, '15:25:00', '15:31:00', 1697, 1),
(29, 1, 29, '15:42:00', '15:45:00', 1707, 1),
(30, 1, 30, '16:15:00', '16:15:00', 1726, 1),
(60, 2, 1, '15:30:00', '15:30:00', 0, 1),
(59, 2, 2, '14:21:00', '14:24:00', 56, 1),
(58, 2, 3, '13:44:00', '13:47:00', 87, 1),
(57, 2, 4, '13:09:00', '13:12:00', 115, 1),
(56, 2, 5, '11:53:00', '11:56:00', 175, 1),
(55, 2, 6, '11:29:00', '11:31:00', 197, 1),
(54, 2, 7, '10:09:00', '10:11:00', 279, 1),
(53, 2, 8, '09:21:00', '09:28:00', 319, 1),
(52, 2, 9, '08:54:00', '08:57:00', 340, 1),
(51, 2, 10, '07:57:00', '08:00:00', 387, 1),
(50, 2, 11, '06:52:00', '06:55:00', 436, 1),
(49, 2, 12, '05:51:00', '05:54:00', 482, 1),
(48, 2, 13, '04:43:00', '04:55:00', 522, 1),
(47, 2, 14, '04:07:00', '04:10:00', 551, 1),
(46, 2, 15, '02:53:00', '02:56:00', 622, 1),
(45, 2, 16, '01:34:00', '01:39:00', 688, 1),
(44, 2, 17, '22:49:00', '23:04:00', 791, 0),
(43, 2, 18, '21:28:00', '21:31:00', 865, 0),
(42, 2, 19, '20:21:00', '20:24:00', 928, 0),
(41, 2, 20, '17:12:00', '17:27:00', 1096, 0),
(40, 2, 21, '15:34:00', '15:37:00', 1198, 0),
(39, 2, 22, '14:11:00', '14:14:00', 1281, 0),
(38, 2, 23, '13:18:00', '13:33:00', 1315, 0),
(37, 2, 24, '11:45:00', '11:48:00', 1408, 0),
(36, 2, 25, '09:28:00', '09:31:00', 1551, 0),
(35, 2, 26, '08:29:00', '08:31:00', 1603, 0),
(34, 2, 27, '07:43:00', '07:45:00', 1649, 0),
(33, 2, 28, '06:41:00', '06:44:00', 1697, 0),
(32, 2, 29, '06:28:00', '06:30:00', 1707, 0),
(31, 2, 30, '06:00:00', '06:00:00', 1726, 0),
(61, 3, 1, '22:45:00', '22:45:00', 319, 0),
(62, 3, 3, '00:06:00', '00:09:00', 232, 1),
(63, 3, 5, '02:51:00', '02:54:00', 144, 1),
(64, 3, 7, '04:30:00', '04:33:00', 40, 1),
(65, 3, 8, '05:15:00', '05:15:00', 0, 1),
(68, 4, 1, '05:10:00', '05:10:00', 319, 1),
(67, 4, 7, '22:45:00', '22:48:00', 40, 0),
(66, 4, 8, '22:05:00', '22:05:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Transaction`
--

CREATE TABLE `Transaction` (
  `id` int(11) NOT NULL,
  `customerId` int(11) DEFAULT NULL,
  `status` enum('prepay','paying','success','failure') COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Transaction`
--

INSERT INTO `Transaction` (`id`, `customerId`, `status`, `total`) VALUES
(1, NULL, 'prepay', '820000.00'),
(2, 1, 'success', '820000.00'),
(3, 2, 'success', '1640000.00'),
(4, 2, 'success', '820000.00'),
(5, 5, 'paying', '820000.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Authentication`
--
ALTER TABLE `Authentication`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `Carriage`
--
ALTER TABLE `Carriage`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no` (`no`,`trainId`),
  ADD KEY `Carriage_fk1` (`trainId`),
  ADD KEY `Carriage_fk0` (`type`);

--
-- Indexes for table `CarriageType`
--
ALTER TABLE `CarriageType`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- Indexes for table `Cart`
--
ALTER TABLE `Cart`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Cart_unique0` (`carriageId`,`seatNo`,`expiryDate`),
  ADD KEY `Cart_fk2` (`startStation`),
  ADD KEY `Cart_fk3` (`endStation`),
  ADD KEY `Cart_fk4` (`transactionId`);

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `Passenger`
--
ALTER TABLE `Passenger`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `Price`
--
ALTER TABLE `Price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Price_fk0` (`type`);

--
-- Indexes for table `Route`
--
ALTER TABLE `Route`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Route_fk1` (`startStation`),
  ADD KEY `Route_fk2` (`endStation`);

--
-- Indexes for table `Station`
--
ALTER TABLE `Station`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `Ticket`
--
ALTER TABLE `Ticket`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Cart_unique0` (`carriageId`,`seatNo`,`expiryDate`,`returnDate`) USING BTREE,
  ADD KEY `Ticket_fk2` (`startStation`),
  ADD KEY `Ticket_fk3` (`endStation`),
  ADD KEY `Ticket_fk4` (`transactionId`),
  ADD KEY `Ticket_fk5` (`passengerId`);

--
-- Indexes for table `Train`
--
ALTER TABLE `Train`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label` (`label`),
  ADD KEY `Train_fk0` (`routeId`);

--
-- Indexes for table `TrainRouting`
--
ALTER TABLE `TrainRouting`
  ADD PRIMARY KEY (`trainId`,`stationId`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `TrainRouting_fk1` (`stationId`);

--
-- Indexes for table `Transaction`
--
ALTER TABLE `Transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Transaction_fk0` (`customerId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Carriage`
--
ALTER TABLE `Carriage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `CarriageType`
--
ALTER TABLE `CarriageType`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Cart`
--
ALTER TABLE `Cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `Customer`
--
ALTER TABLE `Customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `Passenger`
--
ALTER TABLE `Passenger`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Price`
--
ALTER TABLE `Price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT for table `Route`
--
ALTER TABLE `Route`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Station`
--
ALTER TABLE `Station`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `Ticket`
--
ALTER TABLE `Ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Train`
--
ALTER TABLE `Train`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `TrainRouting`
--
ALTER TABLE `TrainRouting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `Transaction`
--
ALTER TABLE `Transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Carriage`
--
ALTER TABLE `Carriage`
  ADD CONSTRAINT `Carriage_fk0` FOREIGN KEY (`type`) REFERENCES `CarriageType` (`id`),
  ADD CONSTRAINT `Carriage_fk1` FOREIGN KEY (`trainId`) REFERENCES `Train` (`id`);

--
-- Constraints for table `Cart`
--
ALTER TABLE `Cart`
  ADD CONSTRAINT `Cart_fk0` FOREIGN KEY (`carriageId`) REFERENCES `Carriage` (`id`),
  ADD CONSTRAINT `Cart_fk2` FOREIGN KEY (`startStation`) REFERENCES `Station` (`id`),
  ADD CONSTRAINT `Cart_fk3` FOREIGN KEY (`endStation`) REFERENCES `Station` (`id`),
  ADD CONSTRAINT `Cart_fk4` FOREIGN KEY (`transactionId`) REFERENCES `Transaction` (`id`);

--
-- Constraints for table `Price`
--
ALTER TABLE `Price`
  ADD CONSTRAINT `Price_fk0` FOREIGN KEY (`type`) REFERENCES `CarriageType` (`id`);

--
-- Constraints for table `Route`
--
ALTER TABLE `Route`
  ADD CONSTRAINT `Route_fk1` FOREIGN KEY (`startStation`) REFERENCES `Station` (`id`),
  ADD CONSTRAINT `Route_fk2` FOREIGN KEY (`endStation`) REFERENCES `Station` (`id`);

--
-- Constraints for table `Ticket`
--
ALTER TABLE `Ticket`
  ADD CONSTRAINT `Ticket_fk0` FOREIGN KEY (`carriageId`) REFERENCES `Carriage` (`id`),
  ADD CONSTRAINT `Ticket_fk2` FOREIGN KEY (`startStation`) REFERENCES `Station` (`id`),
  ADD CONSTRAINT `Ticket_fk3` FOREIGN KEY (`endStation`) REFERENCES `Station` (`id`),
  ADD CONSTRAINT `Ticket_fk4` FOREIGN KEY (`transactionId`) REFERENCES `Transaction` (`id`),
  ADD CONSTRAINT `Ticket_fk5` FOREIGN KEY (`passengerId`) REFERENCES `Passenger` (`id`);

--
-- Constraints for table `Train`
--
ALTER TABLE `Train`
  ADD CONSTRAINT `Train_fk0` FOREIGN KEY (`routeId`) REFERENCES `Route` (`id`);

--
-- Constraints for table `TrainRouting`
--
ALTER TABLE `TrainRouting`
  ADD CONSTRAINT `TrainRouting_fk0` FOREIGN KEY (`trainId`) REFERENCES `Train` (`id`),
  ADD CONSTRAINT `TrainRouting_fk1` FOREIGN KEY (`stationId`) REFERENCES `Station` (`id`);

--
-- Constraints for table `Transaction`
--
ALTER TABLE `Transaction`
  ADD CONSTRAINT `Transaction_fk0` FOREIGN KEY (`customerId`) REFERENCES `Customer` (`id`);

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `clearCartEvent` ON SCHEDULE EVERY 1 MINUTE STARTS '2018-05-06 11:15:46' ON COMPLETION NOT PRESERVE ENABLE DO CALL RailwayReservation.clearCart(1,2)$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
