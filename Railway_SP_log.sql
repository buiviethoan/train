

    -- getStationsAndTrains
    -- return all stations and trains that go over that station
    -- for auto completion
    
    -- usage: call getStationsAndTrains()
    DELIMITER //
    CREATE OR REPLACE PROCEDURE getStationsAndTrains()
        BEGIN 
            SELECT `Station`.`id`,`Station`.`name`,GROUP_CONCAT(`trainId`) `trains`
            FROM `TrainRouting`
            INNER JOIN `Station` ON `Station`.`id` = `TrainRouting`.`stationId`
            GROUP BY `stationId`;
        END;
        //
        
    DELIMITER ;


    -- getStationsFromTrains
    -- params: stationId and trainIds
    -- return all stations that given trains go over
    -- for auto completions

    -- usage: call getStationsFromAStation(1,'1,2,3,4')
    DELIMITER //
    CREATE OR REPLACE PROCEDURE getStationsFromAStation(IN in_stationId INT, IN trainIds VARCHAR(255))
        BEGIN 
            SELECT DISTINCT `Station`.`id`,`Station`.`name`
            FROM `TrainRouting`
            INNER JOIN `Station` ON `Station`.`id` = `TrainRouting`.`stationId`
            WHERE `TrainRouting`.`stationId` <> in_stationId  AND FIND_IN_SET(`trainId`,trainIds) > 0;
        END;
        //
        
    DELIMITER ;

    DELIMITER //
    CREATE OR REPLACE PROCEDURE getStationIdByName(IN station_name VARCHAR(255))
        BEGIN 
            SELECT DISTINCT `Station`.`id`,`Station`.`name`
            FROM `Station`
            WHERE `Station`.`name` = station_name;
        END;
        //
        
    DELIMITER ;


    -- getTrainsFromStartStationToEndStation
    -- params: startStationId, endStationId
    -- return all trains go from startStation to endStation

    -- usage: call getTrainsFromStartStationToEndStation(1, 8);
    DELIMITER //
    CREATE OR REPLACE PROCEDURE getTrainsFromStartStationToEndStation(
        IN in_start_station INT, 
        IN in_end_station INT
    )
        BEGIN 
            SELECT
                Train.id, Train.routeId, Train.label, Train.isForward, Train.totalCarriages,
                t1.leaveTime, t2.arriveTime, t2.dayOffset, ABS(t1.distance - t2.distance) distance

            FROM TrainRouting as t1 
                INNER JOIN TrainRouting as t2 
                    ON t1.trainId = t2.trainId 
                    AND t1.stationId = in_start_station 
                    AND t2.stationId = in_end_station
                INNER JOIN Train 
                    ON Train.id = t2.trainId
            WHERE 
                (t1.distance < t2.distance) = Train.isForward;
        END;
        //
        
    DELIMITER ;



    -- getCarriages(IN in_trainIds VARCHAR(255), IN distance INT)
    -- params: startStationId, endStationId
    -- return all trains go from startStation to endStation

    -- usage: call getCarriages('1,3', 175);
    DELIMITER //
    CREATE OR REPLACE PROCEDURE getCarriages(IN in_trainIds VARCHAR(255), IN distance INT)
        BEGIN 
            SELECT Carriage.id, Carriage.no, Carriage.trainId, Carriage.totalSeats, 
                CarriageType.type,
                Price.pricePerKm * distance price
            FROM Carriage
                INNER JOIN Price
                    ON Carriage.type = Price.type
                    AND FIND_IN_SET(Carriage.trainId,in_trainIds) > 0
                    AND distance >= Price.lowerBound 
                    AND distance < Price.upperBound
                INNER JOIN CarriageType
                    ON Price.type = CarriageType.type;
        END;
        //
        
    DELIMITER ;



    -- getTrainsFromStartStationToEndStation
    -- params: startStationId, endStationId
    -- return all seats that unvailable to choose

    -- usage: call getUnavaiableSeatsInCarriage(45,'2018-05-05', 10,20);
    DELIMITER //
    CREATE OR REPLACE PROCEDURE getUnavaiableSeatsInCarriage(
        IN in_carriageId INT, 
        IN in_dateTime DATE, 
        IN in_lifetime INT,
        IN in_lifetime2 INT
    )
        BEGIN 
            
            SELECT Cart.transactionId, Cart.seatNo seatNo, 1 isInCart
            FROM Cart
            INNER JOIN Transaction
                -- ON Cart.transactionId = Transaction.id
            WHERE Cart.carriageId = in_carriageId 
                AND Cart.expiryDate = in_dateTime
                -- AND Cart.addOn >= DATE_SUB(NOW(), INTERVAL IF(Transaction.status = 'paying',in_lifetime2, in_lifetime) MINUTE)

            UNION 

            SELECT Ticket.transactionId , Ticket.seatNo seatNo, 0 isInCart
            FROM Ticket
            WHERE Ticket.carriageId = in_carriageId
                AND Ticket.status <> 'returned'
                AND Ticket.expiryDate = in_dateTime;
        END;
        //
        
    DELIMITER ;


    -- createTransaction(IN in_init_total DECIMAL(10,2))
    -- return transaction that just created
    -- params: 
    -- usage: call createTransaction(820000);
    DELIMITER //
    CREATE OR REPLACE PROCEDURE createTransaction(IN in_init_total DECIMAL(10,2))
        BEGIN 
            INSERT INTO Transaction (status,total) VALUES ('prepay',in_init_total);
            SELECT LAST_INSERT_ID() transactionId;
        END;
        //
        
    DELIMITER ;



    -- addToCart
    -- return transaction that just created
    -- params: 
    -- usage: call addToCart(46,1,'2018-05-04','06:00:00',820000,1,1,30,5);
    DELIMITER //
    CREATE OR REPLACE PROCEDURE addToCart(
        IN in_carriageId INT,
        IN in_seatNo INT,
        IN in_expiryDate DATE,
        IN in_expiryTime TIME,
        IN in_price DECIMAL(8,2),
        IN in_isForward BOOLEAN,
        IN in_startStation INT,
        IN in_endStation INT,
        IN in_transactionId INT
    )
        BEGIN 
            IF NOT EXISTS (
                SELECT Ticket.id 
                FROM Ticket 
                WHERE 
                    Ticket.carriageId = in_carriageId 
                    AND Ticket.seatNo = in_seatNo
                    AND Ticket.expiryDate = in_expiryDate
            ) THEN
                BEGIN
                    INSERT INTO Cart (carriageId, seatNo, expiryDate, expiryTime, price, isForward, startStation, endStation, transactionId, addOn)
                    VALUES (in_carriageId, in_seatNo, in_expiryDate, in_expiryTime, in_price, in_isForward, in_startStation, in_endStation, in_transactionId, NOW());
                    SELECT LAST_INSERT_ID() cartId, in_seatNo seatNo, in_carriageId carriageId;
                END;
            END IF;
        END;
        //
        
    DELIMITER ;



    -- removeFromCart
    -- params: 
    -- usage: call removeFromCart(46,1,'2018-05-04')
    DELIMITER //
    CREATE OR REPLACE PROCEDURE removeFromCart(
        IN in_carriageId INT,
        IN in_seatNo INT,
        IN in_expiryDate DATE
    )
        BEGIN 
            DELETE FROM Cart
            WHERE Cart.carriageId = in_carriageId
                AND Cart.seatNo = in_seatNo
                AND Cart.expiryDate = in_expiryDate;
        END;
        //
        
    DELIMITER ;

DELIMITER //
    CREATE OR REPLACE PROCEDURE removeFromCartById(
        IN in_cartId INT,
    )
        BEGIN 
            DELETE FROM Cart
            WHERE Cart.carriageId = in_cartId;
        END;
        //
        
    DELIMITER ;

    
    -- addPassenger()
    -- return inserted Passenger id
    -- params: 
    -- usage: call addPassenger();
    DELIMITER //
    CREATE OR REPLACE PROCEDURE addPassenger(
        IN in_firstName CHAR(20),
        IN in_lastName CHAR(20),
        IN in_dob DATE,
        IN in_type CHAR(10)
    )
        BEGIN 
            INSERT INTO Passenger (firstName, lastName, dob, type) 
            VALUES (in_firstName, in_lastName, in_dob, in_type);
            SELECT LAST_INSERT_ID() passengerId;
        END;
        //
        
    DELIMITER ;



    -- addCustomer()
    -- return 
    -- params: 
    -- usage: call addCustomer();
    DELIMITER //
    CREATE OR REPLACE PROCEDURE addCustomer(
        IN in_firstName CHAR(20),
        IN in_lastName CHAR(20),
        IN in_phone CHAR(11),
        IN in_email VARCHAR(50),
        IN in_dob DATE,
        IN in_transactionId INT
    )
        BEGIN 
            INSERT INTO Customer (firstName, lastName, phone, email, dob) 
            VALUES (in_firstName, in_lastName, in_phone, in_email, in_dob)  
            ON DUPLICATE KEY 
            UPDATE 
                firstName = in_firstName, 
                lastName = in_lastName, 
                phone = in_phone,
                dob = in_dob;

            SELECT Customer.id
            FROM Customer WHERE Customer.email = in_email;
        END;
        //
        
    DELIMITER ;


    -- updateTransaction()
    -- return 
    -- params: 
    -- usage: call updateTransaction(5,'paying',3);
    DELIMITER //
    CREATE OR REPLACE PROCEDURE updateTransaction(
       IN in_transactionId INT,
       IN in_status CHAR(10),
       IN in_total DECIMAL(10,2),
       IN in_customerId INT
    )
        BEGIN 
            UPDATE Transaction
            SET Transaction.status = in_status, 
                Transaction.customerId = in_customerId,
                Transaction.total = in_total
            WHERE Transaction.id = in_transactionId;
        END;
        //
        
    DELIMITER ;





    -- createTicket
    -- return 
    -- params: 
    -- usage: call createTicket();
    DELIMITER //
    CREATE OR REPLACE PROCEDURE createTicket(
        IN in_carriageId INT,
        IN in_seatNo INT,
        IN in_expiryDate DATE,
        IN in_expiryTime TIME,
        IN in_price DECIMAL(8,2),
        IN in_isForward BOOLEAN,
        IN in_startStation INT,
        IN in_endStation INT,
        IN in_transactionId INT,
        IN in_passengerId INT
    )
        BEGIN 
            START TRANSACTION;
                INSERT INTO Ticket (carriageId, seatNo, expiryDate, expiryTime, price, isForward, startStation, endStation, transactionId, status, passengerId)
                VALUES (in_carriageId, in_seatNo, in_expiryDate, in_expiryTime, in_price, in_isForward, in_startStation, in_endStation, in_transactionId, 'unused', in_passengerId);
                CALL removeFromCart(in_carriageId, in_seatNo, in_expiryDate);
            COMMIT;
            SELECT LAST_INSERT_ID() ticketId;
        END;
        //
        
    DELIMITER ;




DELIMITER //
    CREATE OR REPLACE PROCEDURE clearCart(
        IN in_lifetime INT,
        IN in_lifetime2 INT
    )
        BEGIN
            DELETE c.* FROM Cart c
            INNER JOIN Transaction t
                ON c.transactionId = t.id
            WHERE 
                t.status = 'success'
                OR c.addOn < DATE_SUB(NOW(), INTERVAL IF(t.status = 'paying',in_lifetime2, in_lifetime) MINUTE);
        END;
        //
        
    DELIMITER ;

DELIMITER //
    CREATE PROCEDURE getTicketInfo(
        IN in_transactionId INT
    )
        BEGIN
            SELECT Ticket.id, Ticket.seatNo, Ticket.expiryDate, Ticket.expiryTime, Ticket.price,
            Carriage.no, Passenger.firstName, Passenger.lastName, train.label 
            FROM Ticket INNER JOIN Carriage ON Ticket.carriageId = Carriage.id 
            INNER JOIN  Passenger ON Ticket.passengerId = Passenger.id 
            INNER JOIN Train ON Carriage.trainId = train.id 
            WHERE Ticket.transactionId = in_transactionId;
        END;
        //
        
    DELIMITER ;


CREATE OR REPLACE EVENT clearCartEvent
    ON SCHEDULE EVERY 1 MINUTE STARTS NOW()
    DO
        CALL RailwayReservation.clearCart(1,2);
