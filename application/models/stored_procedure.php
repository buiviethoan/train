<?php 


class Stored_procedure extends CI_Model
{
    public function getStationsAndTrains()
    {
        $query = $this->db->query("CALL getStationsAndTrains()");
        return $query->result();
    }
    
    public function getStationsFromAStation($in_stationId = '', $trainIds = '')
    {
        $query = $this->db->query("CALL getStationsFromAStation($in_stationId,'$trainIds')");
        return $query->result();
    }
    
    public function getStationIdByName($station_name = '')
    {
        $query = $this->db->query("CALL getStationIdByName('$station_name')");
        $res = $query->result();
        $query->next_result(); 
        $query->free_result(); 
        return $res;
    }
    
    public function getTrainsFromStartStationToEndStation($in_start_station = '', $in_end_station = '')
    {
        $query = $this->db->query("CALL getTrainsFromStartStationToEndStation($in_start_station, $in_end_station)");
        $res = $query->result();
        $query->next_result(); 
        $query->free_result(); 
        return $res;
    }
    
    public function getCarriages($in_trainIds = '', $distance = '')
    {
        $query = $this->db->query("CALL getCarriages('$in_trainIds', $distance)");
        $res = $query->result();
        $query->next_result(); 
        $query->free_result(); 
        return $res;
    }
    
    public function getUnavaiableSeatsInCarriage($in_carriageId = '', $in_dateTime = '', $in_lifetime = 10, $in_lifetime2 = 20)
    {
        $query = $this->db->query("CALL getUnavaiableSeatsInCarriage($in_carriageId, '$in_dateTime', $in_lifetime, $in_lifetime2)");
        $res = $query->result();
        $query->next_result(); 
        $query->free_result(); 
        return $res;
    }
    
    public function createTransaction($in_init_total = 0)
    {
        $query = $this->db->query("CALL createTransaction($in_init_total)");
        $res = $query->result();
        $query->next_result(); 
        $query->free_result(); 
        return $res;
    }
    
    public function addToCart($in_carriageId = '', $in_seatNo = '', $in_expiryDate = '', $in_expiryTime = '', $in_price = '', $in_isForward = '', $in_startStation = '', $in_endStation = '', $in_transactionId = '')
    {
        $query = $this->db->query("CALL addToCart($in_carriageId,$in_seatNo,'$in_expiryDate','$in_expiryTime',$in_price,$in_isForward,$in_startStation,$in_endStation,$in_transactionId)");
        $res = $query->result();
        $query->next_result(); 
        $query->free_result(); 
        return $res;
    }
    
    public function removeFromCart($in_carriageId = '', $in_seatNo = '', $in_expiryDate = '')
    {
        $query = $this->db->query("CALL removeFromCart($in_carriageId,$in_seatNo,'$in_expiryDate')");
    }

    public function addCustomer($firstName = '', $lastName = '',$phone = '', $email = '',$dob = '')
    {
        $query = $this->db->query("CALL addCustomer('$firstName','$lastName','$phone','$email','$dob',1)");
        $res = $query->result();
        $query->next_result();
        $query->free_result();
        return $res;
    }

    public function getTicket($idTicket)

    {

        //$string = "id = ";
        $id="id = ";
        $idS = array();
        foreach ($idTicket as $value) {
            $idS[] = $value->cartId;
        }
        $id = implode(" or id = ", $idS);

        $sql = "SELECT price,expiryDate,expiryTime FROM `cart` WHERE id = ".$id;
        $query = $this->db->query($sql);
//        var_dump($sql);
//        die();
//        $query = $this->db->query("CALL getTrainsFromStartStationToEndStation($in_start_station, $in_end_station)");
        $res = $query->result();

        return $res;
    }

    public function addPassenger($firstName = '', $lastName = '',$dob = '', $type)
    {
        $query = $this->db->query("CALL addPassenger('','$lastName','','$type')");
        $res = $query->result();
        $query->next_result();
        $query->free_result();
        return $res;
    }
    public function getTicketInfo($in_transactionId)
    {
        $query = $this->db->query("call getTicketInfo($in_transactionId)");
        return $query->result();
    }
}