<?php 

final class Route {
    public $id;
    public $name;
    public $distance;
    public $startStation; //object Station
    public $endStation; // object Station
    
    public $trains;  // array of object Train

}