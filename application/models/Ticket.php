<?php 

final class Ticket {
    public $id;
    public $carriage;
    public $seatNo;
    public $expiryDate;
    public $expiryTime;
    public $price;
    public $isForward;
    public $startStation;
    public $endStation;
    public $status;
    public $returnDate;
    public $checkinDate;
    public $passenger;
}