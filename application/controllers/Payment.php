<?php
use \Stripe\Stripe as Stripe;
use \Stripe\Charge as Charge;

Stripe::setApiKey("sk_test_30nKicVQ1MbdjlZTTdMKYc8D");

final class Payment extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Stored_procedure');
    }


    private  function getCurrency($currencyCode)	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://www.vietcombank.com.vn/exchangerates/ExrateXML.aspx");
		
		curl_setopt($ch, CURLOPT_HEADER, 0);  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		
		$test = curl_exec($ch);
		curl_close($ch);

		if (isset($test))
		{
			$xml=simplexml_load_string($test);
			foreach($xml->Exrate as $currents)
			{						
				$current =$currents->attributes();
				if ($current->CurrencyCode == $currencyCode)
				{
					return $current->Sell;
				}
			}
		}
		return null;
    }
    

    private function addCustomer() {
        // add customer first then update transaction
        // call addCusomter(params) to get customerId
        // call updateTransaction(id,'paying',total,cusomterId) 

    }


    private function addPassenger() {
        

    }

    // private function 

    public function stripeTest() {
        $stripeToken = $this->input->get('token');

        $this->addCustomer();
        $this->addPassenger();

        $exRate = $this->getCurrency("USD");
        $result = null;
        
        if ($exRate) {
            $total = $this->input->post('total') * $exRate;
        } else {
            // handle error
        }
        

        try {

            $result = Charge::create([
                'amount' => $total,
                'currency' => 'usd',
                'source' => $stripeToken
            ]);

            if ($result->status == 'success') {
                //update transaction then instert to Ticket
            }

        }  catch (Exception $e) {
            $result = $e->getMessage();
        }
  
        echo $result;
        
    }
}