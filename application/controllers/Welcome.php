<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Stored_procedure');
        $this->load->helper('base_helper');
        $this->load->library('session');
    }


    public function index()
    {
        $data['stations'] = $this->Stored_procedure->getStationsAndTrains();
        $this->load->view('user/index', $data);
    }
    
    public function diem_den()
    {
        $station = $this->input->post('station');
        $trains  = $this->input->post('trains');
        $data = $this->Stored_procedure->getStationsFromAStation($station, $trains);
        echo json_encode($data);
    }
    
    public function searchTrain()
    {
        
        // translate to english
//        $start_station_input = ($this->input->post('gaDi'))  ? translate($this->input->post('gaDi'))  : '';
//        $end_station_input   = ($this->input->post('gaDen')) ? translate($this->input->post('gaDen')) : '';
        $datetime            = ($this->input->post('ngayDi')) ? new \DateTime($this->input->post('ngayDi')) : new \DateTime('now');
        
        // get station's id by its name
        // $start_station       = $this->Stored_procedure->getStationIdByName($start_station_input);
        // $end_station         = $this->Stored_procedure->getStationIdByName($end_station_input);
        
        // $id_end   = 0;
        // $id_start = 0;
        // if(!empty($end_station)){
        //     $id_end = $end_station[0]->id; 
        // }
        // if(!empty($start_station)){
        //     $id_start = $start_station[0]->id;
        // }

        $id_start = $this->input->post('fromId');
        $id_end = $this->input->post('toId');
        $toName = $this->input->post("gaDen");
        $fromName = $this->input->post("gaDi");
        // get trains by start station's id and end station's id
        $data['trains']      = $this->Stored_procedure->getTrainsFromStartStationToEndStation($id_start, $id_end);
        $distance = (!empty($data['trains'])) ? $data['trains'][0]->distance : 0;
        
        $id_trains = '';
        if(!empty($data['trains'])){
            foreach ($data['trains'] as $e){
                $id_trains .= $e->id.",";
            }
            $id_trains = substr($id_trains, 0, -1);
        }
        
        $data['carriages']   = $this->Stored_procedure->getCarriages($id_trains, $distance);
        $data['datetime'] = date_format($datetime, "Y-m-d");
        $data['id_start'] = $id_start;
        $data['id_end']   = $id_end;
//        echo "<pre>";
//        die(var_dump($data['carriages']));
//        echo "</pre>";


        // bullshit

        //
        $_SESSION['fromName'] = $fromName;
        $_SESSION['toName'] = $toName;

        $this->load->view('user/search_result', $data);
    }
    
    public function buyTicket()
    {
        $data["fromName"] = $_SESSION['fromName'];
        $data["toName"] = $_SESSION['toName'];
        $data["tickets"] = $this->Stored_procedure->getTicket(json_decode($_COOKIE['lc']));

        $this->load->view('user/buy_ticket',$data);
    }

    public function getUnavaiableSeats()
    {
        $in_carriageId = ($this->input->post('in_carriageId'))  ? $this->input->post('in_carriageId') : '';
        $in_dateTime = ($this->input->post('in_dateTime'))  ? $this->input->post('in_dateTime') : '';
        $data = $this->Stored_procedure->getUnavaiableSeatsInCarriage($in_carriageId, $in_dateTime, 10, 20);
        echo json_encode($data);
    }
    
    public function createTransaction()
    {
        $in_init_total = ($this->input->post('in_init_total')) ? $this->input->post('in_init_total') : 0;
        $data = $this->Stored_procedure->createTransaction($in_init_total);
        echo json_encode($data);
    }
    
    public function addToCart()
    {
        $in_carriageId = ($this->input->post('in_carriageId')) ? $this->input->post('in_carriageId') : 0;
        $in_seatNo = ($this->input->post('in_seatNo')) ? $this->input->post('in_seatNo') : 0;
        $in_expiryDate = ($this->input->post('in_expiryDate')) ? $this->input->post('in_expiryDate') : 0;
        $in_expiryTime = ($this->input->post('in_expiryTime')) ? $this->input->post('in_expiryTime') : 0;
        $in_price = ($this->input->post('in_price')) ? $this->input->post('in_price') : 0;
        $in_isForward = ($this->input->post('in_isForward')) ? $this->input->post('in_isForward') : 0;
        $in_startStation = ($this->input->post('in_startStation')) ? $this->input->post('in_startStation') : 0;
        $in_endStation = ($this->input->post('in_endStation')) ? $this->input->post('in_endStation') : 0;
        $in_transactionId = ($this->input->post('in_transactionId')) ? $this->input->post('in_transactionId') : 0;
        
        $data = $this->Stored_procedure->addToCart($in_carriageId,$in_seatNo,$in_expiryDate,$in_expiryTime,$in_price,$in_isForward,$in_startStation,$in_endStation,$in_transactionId);
     
        echo json_encode($data);
    }
    
    public function removeFromCart()
    {
        $in_carriageId = ($this->input->post('in_carriageId')) ? $this->input->post('in_carriageId') : 0;
        $in_seatNo = ($this->input->post('in_seatNo')) ? $this->input->post('in_seatNo') : 0;
        $in_expiryDate = ($this->input->post('in_expiryDate')) ? $this->input->post('in_expiryDate') : 0;
        
        $this->Stored_procedure->removeFromCart($in_carriageId,$in_seatNo,$in_expiryDate);
        echo json_encode('success');
    }
    
    public function addCustomerUser()
    {
        $fullName = ($this->input->post('fullName')) ? $this->input->post('fullName') : 0;
        $email = ($this->input->post('email')) ? $this->input->post('email') : 0;
        $phoneNumber = ($this->input->post('phoneNumber')) ? $this->input->post('phoneNumber') : 0;
        $data = $this->Stored_procedure->addCustomer('',$fullName,$phoneNumber,$email,'',1);
        echo json_encode('success - addcustomer');


//        $data = $this->Stored_procedure->addPassenger('a','b','c','d');
//        echo json_encode('success - add passenger');
        $index = 0;
        foreach ($this->input->post() as $key => $value) {
            if (strpos($key, 'name') !== false)
                $index +=1;
        }

        for($i=1;$i<=$index;$i++){
            $fullName = ($this->input->post('name-'.$i)) ? $this->input->post('name-'.$i) : 0;
            $type = ($this->input->post('type-'.$i)) ? $this->input->post('type-'.$i) : 0;
            switch ($type){
                case 0:
                    $type = "adult";
                    break;
                case 1:
                    $type = "children";
                    break;
                case 2:
                    $type = "student";
                    break;
                case 3:
                    $type = "oldster";
                    break;
                default:
                    $type = "adult";
                    break;
            };
            $data = $this->Stored_procedure->addPassenger('', $fullName,'', $type);
            echo json_encode('success - addpassenger'.$i);
        }
    }

    
}
