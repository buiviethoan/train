<!DOCTYPE html>
<html lang="vi" cversion="1.0.29">
<head>
    <!-- Standard meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0;">
    <meta name="description" content="Tìm vé tàu, đặt vé tàu trực tuyến, mua vé tàu trực tuyến, tra cứu thông tin hành trình về giờ tàu và giá vé, tra cứu lại thông tin vé đã đặt">
    <meta name="author" content="Nhóm 5 anh em siêu nhân">
    <link rel="shortcut icon" href="favicon.ico">
    
    <title>Tổng công ty đường sắt Việt Nam - Bán vé tàu trực tuyến</title>
    <!-- compiled CSS -->
    <link rel='stylesheet' media='screen and (min-width: 912px)' href="<?php echo base_url()?>assets/css/bootstrap-cerulean.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/content/ETicket-1.0.29.css" />
    <link rel='stylesheet' media='screen and (max-width: 911px)' href="<?php echo base_url()?>assets/css/bootstrap.min.css" />
<!--    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
    <script src="<?php echo base_url()?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery-ui.css">
    <!--<link rel='stylesheet' media='screen and (max-width: 911px)' href="/content/site-mobile.css" />-->
    <style>
        @media (min-width: 1286px) {
            .adv-left {
                float: left;
                display: block;
                position: fixed;
                top: 146px;
                left: calc(50% - 629px);
                left: -webkit-calccalc(50% - 629px);
                left: -moz-calc(50% - 629px);
            }
            .adv-right {
                float: right;
                display: block;
                position: fixed;
                top: 146px;
                right: calc(50% - 629px);
                right: -webkit-calccalc(50% - 629px);
                right: -moz-calc(50% - 629px);
            }
        }

        @media (max-width: 1286px) {
            .adv-left {
                display: none;
            }
            .adv-right {
                display: none;
            }
        }
    </style>
   
    <style>
        .navbar-toggle {
            margin-right: 28px;
        }

        .navbar {
            margin-bottom: 5px;
            margin: -0.5px -15px;
        }

        .navbar-brand {
            padding: 15px 30px;
        }

        .navbar-nav > li > a {
            padding-left: 30px;
            color: white;
        }

        .navbar-default .navbar-brand {
            color: white;
        }

        .navbar-default .navbar-nav > li > a {
            color: #f1f1f1;
            transition: 0.5s;
        }

        .navbar-default .navbar-nav > li:hover {
            background-color: #1995dc;
        }

        .navbar-nav {
            margin: 0.5px -15px;
        }

        .navbar-default .navbar-toggle .icon-bar {
            background-color: #f1f1f1;
        }

        .cart-item {
            position: absolute;
            right: 13px;
            top: 30px;
            background: #f89406;
            color: #ffffff;
            padding: 2px 5px;
            display: inline-block;
            border-radius: 16px;
            font-size: 9px;
            z-index: 1000;
        }

        .fa {
            display: inline-block;
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            line-height: 1;
        }

        .visible-xs {
            display: block !important;
        }
    </style>
</head>
<body>
    <div class="super-container">
        <div id="menu-fixed">
            <div class="visible-print">
                <h3 style="border-bottom: solid 1px #ccc;padding-bottom: 6px;margin-bottom: -20px;">Tổng công ty Đường sắt Việt Nam</h3>
            </div>
            <div class="et-banner hidden-print">
                <div class="container et-banner" style="max-width:1024px;">
                    <div class="pull-left banner-logo">
                        <div class="pull-left banner-logo"><img src="<?php echo base_url()?>assets/images/LOGO_n.png" /></div>
                        <div class="pull-left" id="bannerDate"></div>
                    </div>
                    <div class="pull-right banner-language">
                        <div class="text-right">
                        </div>
                        <div class="text-right banner-logo-2" style="padding-top: 20px;"><img src="<?php echo base_url()?>assets/images/fpt-logo2.png" width="75" height="48" /></div>
                        <div class="text-right ticket-cart-number" style="padding-top: 6px; display:none;">
                            <div et-ticket-cart-number></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
<!-- TOP MENU -->
        <?php require_once("top_menu.php");?>
<!-- END TOP MENU -->

        <div class="adv-left">
            <a target="_blank" href="http://www.vr.com.vn/cam-nang-di-tau/khuyen-cao-khach-hang-chu-y-khi-mua-ve-truc-tuyen.html">
                <img src="<?php echo base_url()?>assets/images/dsvn1.jpg" />
            </a>
        </div>
        <div class="container et-main-content">
<!--CONTENT-->
            <?php $this->load->view('user/search', $stations) ?>
<!--END CONTENT-->
            <div data-ng-view class="shuffle-animation" style="padding-right:0px; padding-left:0px;"></div>
            <div class="et-col-md-12 et-footer hidden-print">
                <div class="et-footer-menu text-center">
                    <a href="index.html#/">Tìm vé</a>&nbsp;|&nbsp;
                    <a href="index.html#/thongtingiaodich">Thông tin đặt chỗ</a>&nbsp;|&nbsp;
                    <a href="http://k.vnticketonline.vn/#/thongtinhanhtrinh/gadi" target="_blank" >Giờ tàu - Giá vé</a>&nbsp;|&nbsp;
                    <a href="index.html#/huongdan" >Hướng dẫn</a>&nbsp;|&nbsp;
                    <a href="index.html#/lienhe" >Liên hệ</a>
                </div>
                <!-- <div class="row" style="margin-top:20px;">

                    <div class="et-col-md-12 et-footer-logo">
                        <div class="et-col-md-12 text-center" style="font-size: 10px; color: #999;">
                            <div ng-bind-html="'Footer_line1'|translate">Tổng công ty Đường sắt Việt Nam. Số 118 Lê Duẩn, Hoàn Kiếm, Hà Nội. Điện thoại: 19006469. Email: dsvn@vr.com.vn.</div>
                            <div ng-bind-html="'Footer_line2'|translate">Giấy chứng nhận ĐKKD số 113642 theo QĐ thành lập số 973/QĐ-TTg ngày 25/06/2010 của Thủ tướng Chính phủ.</div>
                            <div ng-bind-html="'Footer_line3'|translate">Mã số doanh nghiệp: 0100105052, đăng ký lần đầu ngày 26/07/2010, đăng ký thay đổi lần 4 ngày 27/06/2014 tại Sở KHĐT Thành phố Hà Nội.</div>
                        </div>
                    </div>

                    <div class="et-col-md-12 text-center" style="font-size: 10px; margin-top:15px; color: #999;">
                        <img src="images/fptlogo.png" width="28" height="17" />
                        Copyright by FPT Technology Solutions
                    </div>
                </div> -->
            </div>
        </div>
        <div class="adv-right">
            <a target="_blank" href="http://www.vr.com.vn/">
                <img src="<?php echo base_url()?>assets/images/dsvn2.jpg" />
            </a>
        </div>
    </div>
	
    <!-- compiled JavaScript -->
<!--    <script type="text/javascript" src="ETicket-1.0.29.js.min.js"></script>-->

    <!-- <script type='text/javascript'>
        window._sbzq || function (e) {
            e._sbzq = [];
            var t = e._sbzq;
            t.push(["_setAccount", 13269]);
            var n = e.location.protocol == "https:" ? "https:" : "http:";
            var r = document.createElement("script");
            r.type = "text/javascript";
            r.async = true;
            r.src = n + "//static.subiz.com/public/js/loader.js";
            var i = document.getElementsByTagName("script")[0];
            i.parentNode.insertBefore(r, i)
        }(window);
    </script>
    <script type='text/javascript'>jQuery(document).ready(function () { window.setInterval(function () { jQuery("div.sbzon").addClass("hidden-print"); }, 5000); });</script>
    <script type="text/javascript">
        function get_browser_info() {
            var e, r = navigator.userAgent, i = r.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            return /trident/i.test(i[1]) ? (e = /\brv[ :]+(\d+)/g.exec(r) || [], { name: "IE", version: e[1] || "" }) : "Chrome" === i[1] && (e = r.match(/\bOPR\/(\d+)/), null != e) ? { name: "Opera", version: e[1] } : (i = i[2] ? [i[1], i[2]] : [navigator.appName, navigator.appVersion, "-?"], null != (e = r.match(/version\/(\d+)/i)) && i.splice(1, 1, e[1]), { name: i[0], version: i[1] })
        }

        function is_mobile() {
            var e = ["android", "webos", "iphone", "ipad", "blackberry"];
            for (i in e) if (navigator.userAgent.match("/" + e[i] + "/i")) return !0;
            return !1
        }

        jQuery(document).ready(function () {
            if (is_mobile()) jQuery("#menu-print").hide();
            else {
                var e = get_browser_info();
                "Chrome" == e.name && screen.width == window.innerWidth && screen.height == window.innerHeight ? (jQuery("#menu-print").show(), jQuery("#invehref").attr("href", KioskPrintUrl)) : jQuery("#menu-print").hide()
            }
        });
    </script> -->
    <script>
        jQuery(document).ready(function () {
            jQuery('.nav-menu').click(function () {
                jQuery("#bs-example-navbar-collapse-1").removeClass("show");
            });

        });
    </script>
</body>

</html>