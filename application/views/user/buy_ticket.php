
<!DOCTYPE html>
<html lang="vi" cversion="1.0.29">
<head>
    <!-- Standard meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0;">
    <meta name="description" content="Tìm vé tàu, đặt vé tàu trực tuyến, mua vé tàu trực tuyến, tra cứu thông tin hành trình về giờ tàu và giá vé, tra cứu lại thông tin vé đã đặt">
    <meta name="author" content="Nhóm 5 anh em siêu nhân">
    <link rel="shortcut icon" href="favicon.ico">
    
    <title>Tổng công ty đường sắt Việt Nam - Bán vé tàu trực tuyến</title>
    <!-- compiled CSS -->
    <link rel='stylesheet' media='screen and (min-width: 912px)' href="<?php echo base_url()?>assets/css/bootstrap-cerulean.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/content/ETicket-1.0.29.css" />
    <link rel='stylesheet' media='screen and (max-width: 911px)' href="<?php echo base_url()?>assets/css/bootstrap.min.css" />
<!--    <script src="--><?php //echo base_url()?><!--assets/js/jquery-3.3.1.min.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/jquery-ui.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/js-cookie.js"></script>-->
<!--    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery-ui.css">
    <script src="<?php echo base_url()?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery-ui.css">
    <!--<link rel='stylesheet' media='screen and (max-width: 911px)' href="/content/site-mobile.css" />-->
    <style>
        @media (min-width: 1286px) {
            .adv-left {
                float: left;
                display: block;
                position: fixed;
                top: 146px;
                left: calc(50% - 629px);
                left: -webkit-calccalc(50% - 629px);
                left: -moz-calc(50% - 629px);
            }
            .adv-right {
                float: right;
                display: block;
                position: fixed;
                top: 146px;
                right: calc(50% - 629px);
                right: -webkit-calccalc(50% - 629px);
                right: -moz-calc(50% - 629px);
            }
        }

        @media (max-width: 1286px) {
            .adv-left {
                display: none;
            }
            .adv-right {
                display: none;
            }
        }
    </style>
   
    <style>
        .navbar-toggle {
            margin-right: 28px;
        }

        .navbar {
            margin-bottom: 5px;
            margin: -0.5px -15px;
        }

        .navbar-brand {
            padding: 15px 30px;
        }

        .navbar-nav > li > a {
            padding-left: 30px;
            color: white;
        }

        .navbar-default .navbar-brand {
            color: white;
        }

        .navbar-default .navbar-nav > li > a {
            color: #f1f1f1;
            transition: 0.5s;
        }

        .navbar-default .navbar-nav > li:hover {
            background-color: #1995dc;
        }

        .navbar-nav {
            margin: 0.5px -15px;
        }

        .navbar-default .navbar-toggle .icon-bar {
            background-color: #f1f1f1;
        }

        .cart-item {
            position: absolute;
            right: 13px;
            top: 30px;
            background: #f89406;
            color: #ffffff;
            padding: 2px 5px;
            display: inline-block;
            border-radius: 16px;
            font-size: 9px;
            z-index: 1000;
        }

        .fa {
            display: inline-block;
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            line-height: 1;
        }

        .visible-xs {
            display: block !important;
        }
        
        .input-hidden {
            position: absolute;
            left: -9999px;
        }

          input[type=radio]:checked + label>img {
            border: 1px solid #fff;
            box-shadow: 0 0 3px 3px #090;
            width: 100%;
            height: 130%;
          }

          /* Stuff after this is only to make things more pretty */
          input[type=radio] + label>img {
            border: 1px dashed #444;
            transition: 500ms all;
            width: 100%;
            height: 130%;
          }

    </style>
</head>
<body>
    <div class="super-container">
        <div id="menu-fixed">
            <div class="visible-print">
                <h3 style="border-bottom: solid 1px #ccc;padding-bottom: 6px;margin-bottom: -20px;">Tổng công ty Đường sắt Việt Nam</h3>
            </div>
            <div class="et-banner hidden-print">
                <div class="container et-banner" style="max-width:1024px;">
                    <div class="pull-left banner-logo">
                        <div class="pull-left banner-logo"><img src="<?php echo base_url()?>assets/images/LOGO_n.png" /></div>
                        <div class="pull-left" id="bannerDate"></div>
                    </div>
                    <div class="pull-right banner-language">
                        <div class="text-right">
                        </div>
<!--                        <div class="text-right banner-logo-2" style="padding-top: 20px;"><img src="<?php //echo base_url()?>assets/images/fpt-logo2.png" width="75" height="48" /></div>-->
                        <div class="text-right ticket-cart-number" style="padding-top: 6px; display:none;">
                            <div et-ticket-cart-number></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
<!-- TOP MENU -->
        <?php require_once("top_menu.php");?>
<!-- END TOP MENU -->

        <div class="adv-left">
            <a target="_blank" href="http://www.vr.com.vn/cam-nang-di-tau/khuyen-cao-khach-hang-chu-y-khi-mua-ve-truc-tuyen.html">
                <img src="<?php echo base_url()?>assets/images/dsvn1.jpg" />
            </a>
        </div>
        <div class="container et-main-content">
<!--CONTENT-->
       <div class="form-group">
          <form id = "payTicket" method = "POST" action="addCustomerUser" >
            <table class="table table-bordered">
                <thead class="et-table-header">
                <tr>
                    <th style="background-color: lavender;width: 33%;" class="ng-binding">Họ tên<!--Họ tên--></th>
                    <th style="width: 120px;background-color: lavender;width: 33%;" class="ng-binding">Thông tin chỗ<!--Thông tin chỗ--></th>
                    <th style="background-color: lavender;width: 33%;" class="ng-binding">Giá vé<!--Giá vé--></th>
<!--                    <th style="background-color: lavender;" class="ng-binding">Giảm đối tượng<!--Giảm đối tượng--></th>
<!--                    <th style="background-color: lavender;" class="ng-binding">Khuyến mại<!--Khuyến mại--></th>
<!--                    <th style="background-color: lavender;" class="ng-binding">Bảo hiểm<!--Bảo hiểm--></th>
<!--                    <th style="background-color: lavender;width: 120px;" class="ng-binding">Thành tiền (VNĐ)<!--Thành tiền (VNĐ)--></th>
<!--                    <th style="background-color: lavender;width: 25px;"></th>-->
                </tr>
                </thead>

<!--                <tr class="et-table-group-header" ng-show="ves.chieuDi.length > 0 || ves.chieuVe.length > 0">-->
<!--                    <td colspan="8">-->
<!--                        <label class="ng-binding">Khứ hồi</label>-->
<!--                    </td>-->
<!--                </tr>-->
<!--                </tbody>-->
<!--                <tbody ng-show="ves.chieuDi.length > 0" class="">-->
<!--                <tr class="et-table-group-header ng-hide" ng-show="ves.khuHoi.length > 0 || ves.chieuVe.length > 0">-->
<!--                    <td colspan="8">-->
<!--                        <label class="ng-binding">Chiều đi</label>-->
<!--                    </td>-->
<!--                </tr>-->

            <?php
            $tongtien = 0;
            $index = 0 ;
            foreach ($tickets as $value) {
                $index+=1;
                ?>

                <tbody ng-show="ves.khuHoi.length > 0" class="ng-hide">
                <tr ng-repeat="ve in ves.chieuDi" ng-class="{'et-error-tr': ve.buyingRuleError || ve.sameNameError}" class="ng-scope">
                    <td class="et-table-cell" style="padding: 0px;border-bottom: solid 2px #ccc;width: 33%;">
                        <div class="input-group input-group-sm" style="margin-bottom: 6px;width: 100%;">
                            <span class="input-group-addon text-left ng-binding" style="width: 84px;">Họ tên</span>
                            <input name=<?php echo "name-".($index); ?> type="text" id="hoten" placeholder="Thông tin hành khách" ng-model="ve.hanhKhach.hoTen" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" ng-class="{'et-error-block': ve.nameMissing}" ng-change="syncRegister($index, false)" style="border-top-right-radius: 4px !important;border-bottom-right-radius: 4px !important;" required="">
                        </div>
                        <div class="input-group input-group-sm" style="margin-bottom: 6px;width: 100%;">
                            <span class="input-group-addon text-left ng-binding" style="width: 84px;">Đối tượng</span>
                            <select name = <?php echo "type-".$index; ?>  class="form-control input-sm ng-pristine ng-valid ng-valid-required" ng-model="ve.hanhKhach.loaiKhach" ng-change="changeLoaiHanhKhach(ve)" ng-class="{'et-error-block': ve.typeMissing}" ng-options="passengerType.TenPTOnline|translate group by passengerType.Nhom|translate for passengerType in passengerTypes" id="pt72077906" style="border-top-right-radius: 4px !important;border-bottom-right-radius: 4px !important;" required=""><option value="0" selected="selected" label="Người lớn">Người lớn</option><option value="1" label="Trẻ em">Trẻ em</option><option value="2" label="Sinh viên">Sinh viên</option><option value="3" label="Người cao tuổi">Người cao tuổi</option></select>
                        </div>
<!--                        <div class="input-group input-group-sm" style="width: 100%;">-->
<!--                            <span class="input-group-addon text-left ng-binding" style="width: 84px;">Số giấy tờ</span>-->
<!--                            <input type="text" id="cmtID" placeholder="Số CMND/ Hộ chiếu/ Ngày tháng năm sinh trẻ em" ng-model="ve.hanhKhach.soCMND" maxlength="19" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" ng-class="{'et-error-block': ve.identityMissing}" ng-disabled="ve.hanhKhach.loaiKhach &amp;&amp; (ve.hanhKhach.loaiKhach.MaPT == 2 || ve.hanhKhach.loaiKhach.MaPT == 99)" ng-change="syncRegister($index, true)" style="border-top-right-radius: 4px !important;border-bottom-right-radius: 4px !important;" required="">-->
<!--                        </div>-->
                    </td>
                    <td style="font-size: 10px;border-bottom: solid 2px #ccc;width: 33%;">
                        <div>
                            <div class="ng-binding">
                                <?php echo $fromName ."-". $toName ?>
                            </div>
                            <div class="ng-binding">
                                <!--                                17/05/2018 06:00-->
                                <?php echo $value->expiryDate ." ". $value->expiryTime ?>
                            </div>
                        </div>
                    </td>
                    <td class="et-table-cell text-right ng-binding" style="border-bottom: solid 2px #ccc;width: 33%;">
                        <?php
                        echo  round($value->price,    -4);
                        $tongtien += round($value->price,    -4);
                        ?>
                    </td>
                </tr>
                </tbody>
                <?php
            }
            ?>

                <tfoot>
                <tr class="info">
                    <td colspan="6">
                        <span class="pull-right"><strong class="ng-binding">Tổng tiền<!--Tổng tiền--></strong></span>
                    </td>
                    <td class="text-right">
                        <strong class="ng-binding">
                            <?php  echo $tongtien ?>
                        </strong>
                    </td>
<!--                    <td>&nbsp;</td>-->
                </tr>
                </tfoot>
            </table>
            <!-- - ------------------------------------------------------------->
            <div class="et-register-block">
                <h5 class="ng-binding">Thông tin người đặt vé</h5>
                <div
                        class="row text-info ng-binding" ng-bind-html="'PBuyTicket_dienGiaiNguoiDat'|translate">Quý khách vui lòng điền đẩy đủ và chính xác các thông tin về người mua vé dưới đây. Các thông tin này sẽ được sử dụng để xác minh người mua vé và lấy vé tại ga trước khi lên tàu theo đúng các quy định của Tổng công ty Đường sắt Việt Nam.
                </div>
                <div class="form-horizontal form-group">
                    <div class="row ng-hide" ng-show="registerNameError || registerEmailRequiredError || registerEmailValidError">
                        <div class="col-xs-4 et-col-md-2"></div>
<!--                        <div class="col-xs-8 et-col-md-4">-->
<!--                            <span class="et-error-label ng-binding ng-hide" ng-show="registerNameError">Hãy nhập họ tên người đăng kí</span>-->
<!--                        </div>-->
                        <div class="col-xs-4 et-col-md-2">
                        </div>
<!--                        <div class="col-xs-8 et-col-md-4">-->
<!--                            <span class="et-error-label ng-binding ng-hide" ng-show="registerIdentityError">Số CMND/Hộ chiếu không hợp lệ</span>-->
<!--                        </div>-->
                    </div>
                    <div class="row">
                        <label class="col-xs-4 et-col-md-2 text-left ng-binding">Họ và tên<span style="color: red">*</span>
                        </label>
                        <div class="col-xs-8 et-col-md-4" style="margin-bottom:5px">
                            <input type="text" name="fullName" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" ng-model="register.hoTen" placeholder="Họ và tên" required="" ng-class="{'et-error-block': registerNameError}">
                        </div>
<!--                        <label class="col-xs-4 et-col-md-2 text-left ng-binding" style="padding-right: 0">Số CMND/Hộ chiếu<span style="color: red">*</span>-->
<!--                        </label>-->
<!--                        <div class="col-xs-8 et-col-md-4">-->
<!--                            <input type="text" name="identity" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" ng-model="register.soCMND" maxlength="20" placeholder="Số CMND/Hộ chiếu" required="" ng-class="{'et-error-block': registerIdentityError}">-->
<!--                        </div>-->
                    </div>
                    <div class="row ng-hide" ng-show="registerEmailReTypeError || registerIdentityError">
                        <div class="col-xs-4 et-col-md-2">
                        </div>
<!--                        <div class="col-xs-8 et-col-md-4">-->
<!--                            <span class="et-error-label ng-binding ng-hide" ng-show="registerEmailRequiredError">Hãy nhập email người đăng kí</span>-->
<!--                            <br>-->
<!--                            <span class="et-error-label ng-binding ng-hide" ng-show="registerEmailValidError">Email không hợp lệ</span>-->
<!--                        </div>-->
                        <div class="col-xs-4 et-col-md-2">
                        </div>
<!--                        <div class="col-xs-8 et-col-md-4"><span class="et-error-label ng-binding ng-hide" ng-show="registerEmailReTypeError">Xác nhận email không khớp</span>-->
<!--                        </div>-->
                    </div>
                    <div class="row">
                        <label class="col-xs-4 et-col-md-2 text-left ng-binding" style="padding-right: 0">Email</label>
                        <div class="col-xs-8 et-col-md-4" style="margin-bottom:5px">
                            <input type="email" id="email" name="email" class="form-control input-sm ng-pristine ng-valid ng-valid-email" ng-model="register.email" placeholder="Email" ng-class="{'et-error-block': (registerEmailRequiredError || registerEmailValidError)}">
                        </div>
<!--                        <label class="col-xs-4 et-col-md-2 text-left ng-binding" style="padding-right: 0">Xác nhận email</label>-->
<!--                        <div class="col-xs-8 et-col-md-4"><input type="email" name="emailConfirm" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required ng-valid-email" ng-model="register.emailReType" placeholder="Xác nhận email" required="" ng-class="{'et-error-block': registerEmailReTypeError}">-->
<!--                        </div>-->
                    </div>
                    <div class="row ng-hide" ng-show="registerPhoneError">
                        <div class="col-xs-4 et-col-md-2">
                        </div>
    <!--                        <div class="col-xs-8 et-col-md-4">-->
    <!--                            <span class="et-error-label ng-binding ng-hide" ng-show="registerPhoneError">Số di động không hợp lệ</span>-->
    <!--                        </div>-->
                    </div>
                    <div class="row">
                        <label class="col-xs-4 et-col-md-2 text-left ng-binding">Số di động</label><div class="col-xs-8 et-col-md-4" style="margin-bottom:5px">
                            <input type="text" id="phoneNumber" name="phoneNumber" class="form-control input-sm ng-pristine ng-valid" ng-model="register.dienThoai" maxlength="20" placeholder="Số di động" ng-class="{'et-error-block': registerPhoneError}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <input type="submit" class="et-btn" value="Thanh Toán >>">
            </div>

          </form>
       </div>

            <script>
                jQuery(document).ready(function () {

                    // const hoten = $_POST["fullName"]
                    // alert( 'sdfsdfsdf : '.hoten )


                    const form  = document.getElementById("payTicket")
                    form.onsubmit = (event) => {
                        const hoten = $("#hoten").val()
                        const cmtID = $("#cmtID").val()
                        const fullName = $("#fullName").val()
                        const email = $("#email").val()
                        const phoneNumber = $("#phoneNumber").val()

                        // if(fromStation.length === 0 || toStation.length === 0) {
                        // alert(fromName)
                        // return false
                        // }

                        // $("#fromId").val(fromStation[0].id)
                        // $("#toId").val(toStation[0].id)

                        return true
                    }
                });
            </script>

        </body>
</html>
        
<!--END CONTENT-->



