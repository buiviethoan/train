<div id="searchScreen" class="row ng-scope">
    <div class="col-xs-12 col-sm-4 et-col-md-3">
        <div class="et-col-md-12 et-widget" style="margin-bottom: 5px">
            <div class="row et-widget-header">
                <img src="<?php echo base_url() ?>"/>
                <span>
                    <strong>Thông tin hành trình</strong>
                </span>
            </div>
            
            <div class="form-group">
                <form id = "searchForm" method="POST" action="welcome/searchTrain">
                    <input id="fromId" type="hidden" name = "fromId" value = "">
                    <input id="toId" type="hidden" name = "toId" value = "">
                    <div class="form-group">
                        <h6>Ga đi</h6> 
                        <input type="text" placeholder="Ga đi" class="form-control input-sm" id="gaDi" name="gaDi">
                        <ul class="dropdown-menu" role="listbox" style="display: none; top: 96px; left: 15px;" id="drop-hint">
                        </ul>
                    </div>
                    <div class="form-group">
                        <h6>Ga đến</h6> 
                        <input type="text" placeholder="Ga đến" class="form-control input-sm" id="gaDen" name="gaDen">
                        <ul class="dropdown-menu" role="listbox" style="display: none; top: 164px; left: 15px;" id="drop-hint2">
                        </ul>
                    </div>
                    <div class="form-group">
                        <input type="radio" value="0" name="oneWay" checked="checked" id="oneWay">
                        <span>Một chiều</span>
                        <input type="radio" value="1" name="oneWay" id="twoWay">
                        <span>Khứ hồi</span>
                    </div>
                    <div class="form-group">
                        <h6>Ngày đi</h6>
                        <input type="text" id="datepicker" placeholder="Ngày đi" class="form-control input-sm" name="ngayDi"></p>
                    </div>
                    <div class="form-group">
                        <h6>Ngày về</h6>
                        <input type="text" id="datepicker2" placeholder="Ngày về" class="form-control input-sm" name="ngayVe" disabled></p>
                    </div>
                    <div class="row text-center">
                        <input type="submit" class="et-btn" value="Tìm kiếm">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo base_url()?>assets/removeUnicode.js"></script>
<script type="text/javascript">
    var stored_procedure2 = [];
    $(document).ready(function(){
    
    $("#datepicker").datepicker();
    $("#datepicker2").datepicker();

    if($("#twoWay").on('click', function(){
        $("#datepicker2").removeAttr('disabled');
    }));

    if($("#oneWay").on('click', function(){
        $("#datepicker2").attr('disabled', true);
    }));
    var a = [];
    <?php foreach ($stations as $entity){ ?>
         var o = {id:"<?php echo $entity->id; ?>", name:"<?php echo $entity->name; ?>", trains:"<?php echo $entity->trains;?>"};
         a.push(o);
    <?php } ?>
        
//    search in array a
    $("#gaDi").on('keyup', function(){
        
        const gaDi_value = $("#gaDi").val()
        const hint = findStationByName(gaDi_value, a)
        $("#drop-hint").css('display', 'block');
        $("#drop-hint").empty();
        for(var i=0; i < hint.length; i++){
            $("#drop-hint").append("<li><a class='hint'>"+hint[i].name+"</a></li>");
        }
        
        $(".hint").on('click', function(){
            $("#gaDi").val(this.innerHTML) ;
            $("#drop-hint").empty();
            $("#drop-hint").css('display', 'none');
            $("#gaDi").blur();
        });
    });
    
    // remove hint when out focus
    $("#gaDi").focusout(function(){
        setTimeout(function(){
            $("#drop-hint").empty();
            $("#drop-hint").css('display', 'none');
        }, 5000);
        
        for(var i=0; i < a.length; i++){
            if(a[i].name === $("#gaDi").val()){
                var ajax_data = a[i];
            }
        }
        
        // stored procedure 2
        if(typeof ajax_data !== 'undefined'){
            $.ajax({
                url: 'welcome/diem_den',
                type: "POST",
                data: {
                    station: ajax_data.id,
                    trains : ajax_data.trains
                },
                success: function(result){
                    var b = JSON.parse(result);
                    stored_procedure2 = b.slice();
                    console.log(stored_procedure2);
                }
            });            
        }
    });
    
    $("#gaDen").on('keyup', function(){
        const gaDen_val = $("#gaDen").val();
        const hint = findStationByName(gaDen_val, stored_procedure2)
        $("#drop-hint2").css('display', 'block');
        $("#drop-hint2").empty();
        for(var i=0; i < hint.length; i++){
            $("#drop-hint2").append("<li><a class='hint2'>"+hint[i].name+"</a></li>");
        }


        $(".hint2").on('click', function(){
            $("#gaDen").val(this.innerHTML) ;
            $("#drop-hint2").empty();
            $("#drop-hint2").css('display', 'none');
            $("#gaDen").blur();
        });
    });
    
    $("#gaDen").focusout(function(){
        setTimeout(function(){
            $("#drop-hint2").empty();
            $("#drop-hint2").css('display', 'none');
        }, 5000);
    });
    const form  = document.getElementById("searchForm")
    form.onsubmit = (event) => {
        const fromName = $("#gaDi").val()
        const toName = $("#gaDen").val()
        const fromStation = findStationByName(fromName,a)
        const toStation = findStationByName(toName,stored_procedure2)

        if(fromStation.length === 0 || toStation.length === 0) {
            alert("invalidate station!!!")
            return false
        }

        $("#fromId").val(fromStation[0].id)
        $("#toId").val(toStation[0].id)
        
        return true
    }

  });

  function findStationByName(name, stations) {
      const stdName = removeUnicode(name).toLowerCase()
      return stations.filter(station => {
            return removeUnicode(station.name.toLowerCase()).includes(stdName);
        })
  }

</script>