<html lang="vi" cversion="1.0.29">
<head>
    <!-- Standard meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0;">
    <meta name="description" content="Tìm vé tàu, đặt vé tàu trực tuyến, mua vé tàu trực tuyến, tra cứu thông tin hành trình về giờ tàu và giá vé, tra cứu lại thông tin vé đã đặt">
    <meta name="author" content="Nhóm 5 anh em siêu nhân">
    <link rel="shortcut icon" href="favicon.ico">
    
    <title>Tổng công ty đường sắt Việt Nam - Bán vé tàu trực tuyến</title>
    <!-- compiled CSS -->
    <link rel='stylesheet' media='screen and (min-width: 912px)' href="<?php echo base_url()?>assets/css/bootstrap-cerulean.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/content/ETicket-1.0.29.css" />
    <link rel='stylesheet' media='screen and (max-width: 911px)' href="<?php echo base_url()?>assets/css/bootstrap.min.css" />
    <script src="<?php echo base_url()?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>assets/js/js-cookie.js"></script>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery-ui.css">
    <style>
        @media (min-width: 1286px) {
            .adv-left {
                float: left;
                display: block;
                position: fixed;
                top: 146px;
                left: calc(50% - 629px);
                left: -webkit-calccalc(50% - 629px);
                left: -moz-calc(50% - 629px);
            }
            .adv-right {
                float: right;
                display: block;
                position: fixed;
                top: 146px;
                right: calc(50% - 629px);
                right: -webkit-calccalc(50% - 629px);
                right: -moz-calc(50% - 629px);
            }
        }

        @media (max-width: 1286px) {
            .adv-left {
                display: none;
            }
            .adv-right {
                display: none;
            }
        }
    </style>
   
    <style>
        .navbar-toggle {
            margin-right: 28px;
        }

        .navbar {
            margin-bottom: 5px;
            margin: -0.5px -15px;
        }

        .navbar-brand {
            padding: 15px 30px;
        }

        .navbar-nav > li > a {
            padding-left: 30px;
            color: white;
        }

        .navbar-default .navbar-brand {
            color: white;
        }

        .navbar-default .navbar-nav > li > a {
            color: #f1f1f1;
            transition: 0.5s;
        }

        .navbar-default .navbar-nav > li:hover {
            background-color: #1995dc;
        }

        .navbar-nav {
            margin: 0.5px -15px;
        }

        .navbar-default .navbar-toggle .icon-bar {
            background-color: #f1f1f1;
        }

        .cart-item {
            position: absolute;
            right: 13px;
            top: 30px;
            background: #f89406;
            color: #ffffff;
            padding: 2px 5px;
            display: inline-block;
            border-radius: 16px;
            font-size: 9px;
            z-index: 1000;
        }

        .fa {
            display: inline-block;
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            line-height: 1;
        }

        .visible-xs {
            display: block !important;
        }
        
        .input-hidden {
            position: absolute;
            left: -9999px;
        }

          input[type=radio]:checked + label>img {
            border: 1px solid #fff;
            box-shadow: 0 0 3px 3px #090;
            width: 100%;
            height: 130%;
          }

          /* Stuff after this is only to make things more pretty */
          input[type=radio] + label>img {
            border: 1px dashed #444;
            transition: 500ms all;
            width: 100%;
            height: 130%;
          }

    </style>
</head>
<body>
    <div class="super-container">
        <div id="menu-fixed">
            <div class="visible-print">
                <h3 style="border-bottom: solid 1px #ccc;padding-bottom: 6px;margin-bottom: -20px;">Tổng công ty Đường sắt Việt Nam</h3>
            </div>
            <div class="et-banner hidden-print">
                <div class="container et-banner" style="max-width:1024px;">
                    <div class="pull-left banner-logo">
                        <div class="pull-left banner-logo"><img src="<?php echo base_url()?>assets/images/LOGO_n.png" /></div>
                        <div class="pull-left" id="bannerDate"></div>
                    </div>
                    <div class="pull-right banner-language">
                        <div class="text-right">
                        </div>
<!--                        <div class="text-right banner-logo-2" style="padding-top: 20px;"><img src="<?php //echo base_url()?>assets/images/fpt-logo2.png" width="75" height="48" /></div>-->
                        <div class="text-right ticket-cart-number" style="padding-top: 6px; display:none;">
                            <div et-ticket-cart-number></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
<!-- TOP MENU -->
        <?php require_once("top_menu.php");?>
<!-- END TOP MENU -->

        <div class="adv-left">
            <a target="_blank" href="http://www.vr.com.vn/cam-nang-di-tau/khuyen-cao-khach-hang-chu-y-khi-mua-ve-truc-tuyen.html">
                <img src="<?php echo base_url()?>assets/images/dsvn1.jpg" />
            </a>
        </div>
        <div class="container et-main-content">
<!--CONTENT-->
        <?php $i = 0; ?>
        <div class="col-xs-12 col-sm-9 et-col-md-9">
            <div class="row et-train-list">
                <div class="train-group">
                    <?php foreach($trains as $train): ?>
                    <div class="col-xs-4 col-sm-3 et-col-md-2 et-train-block">
                        <div class="et-train-head selectTrain" data-train-no="<?php echo $i++; ?>" data-train-id="<?php echo $train->id; ?>">
                            <div class="row center-block" style="width: 40%; margin-bottom: 3px"><div class="et-train-lamp text-center ng-binding"><?php echo $train->label;?></div></div>
                            <div class="et-train-head-info">
                                <div class="row et-no-margin">
                                    <span class="pull-left et-bold">TG đi</span>
                                    <span class="pull-right et-bold"><?php echo $train->leaveTime; ?></span>
                                </div>
                            </div>
                            <div class="row et-no-margin">
                                <div class="et-col-50">
                                    <span class="et-train-lamp-bellow-left"></span>
                                </div>
                                <div class="et-col-50">
                                    <span class="et-train-lamp-bellow-right"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            
            KHOANG: <br><br>
            <div class="row" style="margin-left:-10PX">
                <div class="col-md-12 et-no-margin">
                    <div id="carriages">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <br><br>
                <h6 class="carriage-info" style=" font-size: 13px;font-weight: 700;"></h6>
            </div>
            <br>
            GHẾ: <br>
            <div class="row et-car-floor">
                
            <div id="seats" class="et-col-90">
                
<!--            <input type="checkbox" value="1"><span>1</span>
            <input type="checkbox" value="2"><span>2</span>
            <input type="checkbox" value="3"><span>3</span>-->
            </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3 et-col-md-3 part-right">
            <div class="col-md-12 et-widget" id="ticketPocket" style="padding-bottom: 8px">
                <div class="row et-widget-header">
                    <img src="<?php echo base_url()?>assets/images/widgetIcon.png">
                    <span><strong class="ng-binding">Mua vé tàu</strong></span></div>
                <div class="row basket-ticket">
<!--                    <div class="col-md-12 et-ticket-info">
                        <div class="et-col-84">
                            <div>SE7 Hà Nội-Vinh</div>
                            <div>12/05/2018 06:00</div>
                            <div>NML toa 1 chỗ 20</div>
                        </div>
                    </div>
                    <div class="col-md-12 et-ticket-info">
                        <div class="et-col-84">
                            <div>SE7 Hà Nội-Vinh</div>
                            <div>12/05/2018 06:00</div>
                            <div>NML toa 1 chỗ 20</div>
                        </div>
                    </div>-->
                </div>
                <div class="col-md-12 text-center" style="margin-top: 2px">
<!--                    <button id="btnCheckOut" class="btn btn-sm et-btn">Mua vé</button>-->
                    <a href="buyTicket" id="btnCheckOut" target="" class="btn btn-sm et-btn">Mua vé</a>
                </div>
            </div>
        </div>
        
        <br>
        
            
<!--            <input type="radio" name="carriages" class="carriages" data-carriage-id="49">
            <input type="radio" name="carriages" class="carriages" data-carriage-id="50">-->
        
            
       
        <script type="text/javascript">
            $(document).ready(function(){
                function getCookie(cname) {
                    var name = cname + "=";
                    var decodedCookie = decodeURIComponent(document.cookie);
                    var ca = decodedCookie.split(';');
                    for(var i = 0; i <ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') {
                            c = c.substring(1);
                        }
                        if (c.indexOf(name) == 0) {
                            return c.substring(name.length, c.length);
                        }
                    }
                    return "";
                }
                
                var list_cart = [];
                var carriages = [];
                var trains = [];
                var train = null;
                <?php foreach ($carriages as $entity): ?>
                    var o = {id:"<?php echo $entity->id; ?>", no:"<?php echo $entity->no; ?>", trainId:"<?php echo $entity->trainId;?>", totalSeats:"<?php echo $entity->totalSeats;?>", type:"<?php echo $entity->type;?>", price:"<?php echo $entity->price;?>"};
                    carriages.push(o);
                <?php endforeach; ?>
                    
                <?php foreach ($trains as $entity) : ?>
                    var o = {id:"<?php echo $entity->id ?>", routeId:"<?php echo $entity->routeId ?>", label:"<?php echo $entity->label ?>", isForward:"<?php echo $entity->isForward ?>", totalCarriages:"<?php echo $entity->totalCarriages ?>", leaveTime:"<?php echo $entity->leaveTime ?>", arriveTime:"<?php echo $entity->arriveTime ?>", dayOffset:"<?php echo $entity->dayOffset ?>", distance:"<?php echo $entity->distance ?>"};
                    trains.push(o);
                <?php endforeach; ?>
                
                // thay đổi giá trị tàu
                $(".selectTrain").on('click', function(){
//                    console.log($(".selectTrain:checked").val());
                    $('.selectTrain').removeClass('et-train-head-selected');
                    $(this).toggleClass('et-train-head-selected');
//                    $(this).attr('checked', true);
//                    console.log($(this).attr('data-train-no'));
                    train = trains[$(this).attr('data-train-no')];

                    $("#carriages").empty();
                    $("#seats").empty();
                    // tìm khoang thuộc tàu
                    for(var i = 0; i < carriages.length; i++){
                        if($(this).attr('data-train-id') == carriages[i].trainId){
                           $("#carriages").append("<div class='et-car-block et-car-icon-avaiable et-car-icon' data-carriage-id='"+carriages[i].id+"'><input type='radio' class='input-hidden' name='carriages' class='carriages' id="+carriages[i].id+" data-carriage-id="+carriages[i].id+" data-carriage-no="+carriages[i].no+"><label for='"+carriages[i].id+"'><img src='<?php echo base_url()?>assets/images/trainCar2.png' /></label><div class='text-center text-info et-car-label'>"+carriages[i].no+"</div></div>");
                        }
                    }
                    
                    // thay đổi khoang
                    $(".et-car-block").on('click', function(){
                        var self = $(this);
                        
                        var unavaiable_seat = [];
                            
                            //ajax lấy trạng thái ghế trống
                            $("#seats").empty();    
                            var carriage_id = $(this).attr('data-carriage-id');
                            for(var i = 0; i < carriages.length; i++){
                                if(carriage_id == carriages[i].id){
                                    carriage = carriages[i];
                                }
                            }
                            
                            $(".carriage-info").html("Toa số "+carriage.no+": "+carriage.type+" - Giá vé: "+Math.round(carriage.price/1000)*1000+" VNĐ");
                             
                            // load ghế
                            for(var i = 1; i <= carriage.totalSeats; i++ ){
                                if(i % 20 == 0){
                                    $("#seats").append('<span><input class="select-seat" type="checkbox" value="'+i+'"><span data-seat-no='+i+'>'+i+'</span></span><br>');
                                }else{
                                    $("#seats").append('<span><input class="select-seat" type="checkbox" value="'+i+'"><span data-seat-no='+i+'>'+i+'</span></span>');
                                }
                            }
                            
                            // avaiable
                            $.ajax({
                                url: 'getUnavaiableSeats',
                                type: "POST",
                                data: {
                                    in_carriageId: self.attr('data-carriage-id'),
                                    in_dateTime: '<?php echo $datetime; ?>'
                                },
                                success: function(result){
                                    var b = JSON.parse(result);
                                    unavaiable_seat = b.slice();
                                    for(var i = 0; i < unavaiable_seat.length; i++){
                                        // ghế đã được đặt
                                        if(unavaiable_seat[i].isInCart == 0){
                                            $("input[type=checkbox][value="+unavaiable_seat[i].seatNo+"]").attr('disabled', true);
                                            $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'red');
                                        }
                                        else if (unavaiable_seat[i].isInCart == 1){
                                            console.log("1");
                                            if(unavaiable_seat[i].transactionId == getCookie('tid')){
                                                $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'blue');
                                                $("input[class=select-seat][value="+unavaiable_seat[i].seatNo+"]").attr('checked', true);
                                            }else{
                                                $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'yellow');
                                                $("input[type=checkbox][value="+unavaiable_seat[i].seatNo+"]").attr('disabled', true);
                                            }
                                        }
                                    }
                                }
                            });
                            
                            
                            $(".select-seat").on('click', function(event){
                                var seat_no = this.value;
                                
                                // tạo giao dịch nếu chưa có
                                if(document.cookie.indexOf("tid") < 0){
                                    document.cookie = "lc=''";
                                    $.ajax({
                                       url: 'createTransaction',
                                       type: "POST",
                                       data:{
                                           in_init_total: carriage.price
                                       },
                                       success: function(result){
                                           var c = JSON.parse(result);
                                           var expiry = new Date();
                                           expiry.setTime(expiry.getTime()+(10*60*1000));
                                           document.cookie = "tid="+ c[0].transactionId +"; expires=" + expiry.toGMTString();
                                           $.ajax({
                                                url: "addToCart",
                                                type: "POST",
                                                dataType: "JSON",
                                                data: {
                                                    in_carriageId: carriage.id,
                                                    in_seatNo: seat_no,
                                                    in_expiryDate: '<?php echo $datetime; ?>',
                                                    in_expiryTime: train.leaveTime,
                                                    in_price: carriage.price,
                                                    in_isForward: train.isForward,
                                                    in_startStation: '<?php echo $id_start; ?>',
                                                    in_endStation: '<?php echo $id_end; ?>',
                                                    in_transactionId: c[0].transactionId
                                                },
                                                success: function(result) {
                                                    const temp = result;
                                                    // return cart_id
                                                    // avaiable
                                                    $.ajax({
                                                        url: 'getUnavaiableSeats',
                                                        type: "POST",
                                                        data: {
                                                            in_carriageId: self.attr('data-carriage-id'),
                                                            in_dateTime: '<?php echo $datetime; ?>'
                                                        },
                                                        success: function(result){
                                                            var b = JSON.parse(result);
                                                            unavaiable_seat = b.slice();
                                                            for(var i = 0; i < unavaiable_seat.length; i++){
                                                                // ghế đã được đặt
                                                                if(unavaiable_seat[i].isInCart == 0){
                                                                    $("input[type=checkbox][value="+unavaiable_seat[i].seatNo+"]").attr('disabled', true);
                                                                    $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'red');
                                                                }
                                                                else if (unavaiable_seat[i].isInCart == 1){
                                                                    if(unavaiable_seat[i].transactionId == getCookie('tid')){
                                                                        $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'blue');
                                                                    }else{
                                                                        $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'yellow');
                                                                        $("input[type=checkbox][value="+unavaiable_seat[i].seatNo+"]").attr('disabled', true);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    });
                                                    
                                                    // lưu cookie cart_id
                                                    list_cart = list_cart.concat(temp);
                                                    document.cookie = "lc="+ JSON.stringify(list_cart) +"; expires=" + expiry.toGMTString();
                                                    
                                                }
                                           });
                                       }
                                    });
                                }
                                // đã có cookie lưu transactionId
                                else{
                                    list_cart = JSON.parse(getCookie('lc'));
                                    if(list_cart.length == 4){
                                        if($(this).is(':checked') == false){
                                            $.ajax({
                                                url: "removeFromCart",
                                                type: "POST",
                                                dataType: "JSON",
                                                data: {
                                                    in_carriageId: carriage.id,
                                                    in_seatNo: seat_no,
                                                    in_expiryDate: '<?php echo $datetime;?>'
                                                },
                                                success: function(result){
    //                                                console.log(result);
    //                                                
                                                    // xóa cookie chứa seat_no
                                                    list_cart = list_cart.filter(cart => cart.seatNo != seat_no);
                                                    document.cookie = "lc="+ JSON.stringify(list_cart);
                                                    $("span[data-seat-no="+seat_no+"]").css('color', 'black');
                                                }
                                            });
                                        }else{
                                            event.preventDefault();
                                            alert('Bạn đã đặt đủ 4 vé');
                                        }   
                                    }else{
                                        if($(this).is(':checked') == true){
                                            $.ajax({
                                                url: "addToCart",
                                                type: "POST",
                                                dataType: "JSON",
                                                data: {
                                                    in_carriageId: carriage.id,
                                                    in_seatNo: seat_no,
                                                    in_expiryDate: '<?php echo $datetime; ?>',
                                                    in_expiryTime: train.leaveTime,
                                                    in_price: carriage.price,
                                                    in_isForward: train.isForward,
                                                    in_startStation: '<?php echo $id_start; ?>',
                                                    in_endStation: '<?php echo $id_end; ?>',
                                                    in_transactionId: getCookie('tid')
                                                },
                                                success: function(result) {
                                                    const temp = result;
                                                    // return cart_id
                                                    // avaiable
                                                    $.ajax({
                                                        url: 'getUnavaiableSeats',
                                                        type: "POST",
                                                        data: {
                                                            in_carriageId: self.attr('data-carriage-id'),
                                                            in_dateTime: '<?php echo $datetime; ?>'
                                                        },
                                                        success: function(result){
                                                            var b = JSON.parse(result);
                                                            unavaiable_seat = b.slice();
                                                            for(var i = 0; i < unavaiable_seat.length; i++){
                                                                // ghế đã được đặt
                                                                if(unavaiable_seat[i].isInCart == 0){
                                                                    $("input[type=checkbox][value="+unavaiable_seat[i].seatNo+"]").attr('disabled', true);
                                                                    $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'red');
                                                                }
                                                                else if (unavaiable_seat[i].isInCart == 1){
                                                                    if(unavaiable_seat[i].transactionId == getCookie('tid')){
                                                                        $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'blue');
                                                                    }else{
                                                                        $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'yellow');
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    });

                                                    // lưu cookie cart_id
                                                    list_cart = list_cart.concat(temp);
                                                    document.cookie = "lc="+ JSON.stringify(list_cart);
        //                                                    console.log(list_cart[0].cartId);
                                                }
                                           });
                                        }else{
                                            $.ajax({
                                                url: "removeFromCart",
                                                type: "POST",
                                                dataType: "JSON",
                                                data: {
                                                    in_carriageId: carriage.id,
                                                    in_seatNo: seat_no,
                                                    in_expiryDate: '<?php echo $datetime;?>'
                                                },
                                                success: function(result){
                                                    // xóa cookie chứa seat_no
                                                    list_cart = list_cart.filter(cart => cart.seatNo != seat_no);
                                                    document.cookie = "lc="+ JSON.stringify(list_cart);
                                                    $("span[data-seat-no="+seat_no+"]").css('color', 'black');
                                                    
                                                    // unavaiable seat
                                                    $.ajax({
                                                        url: 'getUnavaiableSeats',
                                                        type: "POST",
                                                        data: {
                                                            in_carriageId: self.attr('data-carriage-id'),
                                                            in_dateTime: '<?php echo $datetime; ?>'
                                                        },
                                                        success: function(result){
                                                            var b = JSON.parse(result);
                                                            unavaiable_seat = b.slice();
                                                            for(var i = 0; i < unavaiable_seat.length; i++){
                                                                // ghế đã được đặt
                                                                if(unavaiable_seat[i].isInCart == 0){
                                                                    $("input[type=checkbox][value="+unavaiable_seat[i].seatNo+"]").attr('disabled', true);
                                                                    $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'red');
                                                                }
                                                                else if (unavaiable_seat[i].isInCart == 1){
                                                                    if(unavaiable_seat[i].transactionId == getCookie('tid')){
                                                                        $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'blue');
                                                                    }else{
                                                                        $("span[data-seat-no="+unavaiable_seat[i].seatNo+"]").css('color', 'yellow');
                                                                        $("input[type=checkbox][value="+unavaiable_seat[i].seatNo+"]").attr('disabled', true);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    });
                                                    
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                            
                        });
                });
            });
        </script>
<!--END CONTENT-->
        <div data-ng-view class="shuffle-animation" style="padding-right:0px; padding-left:0px;"></div>
<!--            <div class="et-col-md-12 et-footer hidden-print">
                <div class="et-footer-menu text-center">
                    <a href="">Tìm vé</a>&nbsp;|&nbsp;
                    <a href="">Thông tin đặt chỗ</a>&nbsp;|&nbsp;
                    <a href="" target="_blank" >Giờ tàu - Giá vé</a>&nbsp;|&nbsp;
                    <a href="" >Hướng dẫn</a>&nbsp;|&nbsp;
                    <a href="" >Liên hệ</a>
                </div>
            </div>-->
        </div>
        <div class="adv-right">
            <a target="_blank" href="http://www.vr.com.vn/">
                <img src="<?php echo base_url()?>assets/images/dsvn2.jpg" />
            </a>
        </div>
    </div>
    <script>
        jQuery(document).ready(function () {
            jQuery('.nav-menu').click(function () {
                jQuery("#bs-example-navbar-collapse-1").removeClass("show");
            });

        });
    </script>
</body>

</html>