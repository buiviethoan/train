<html cversion="1.0.29" lang="vi"><head><style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}.ng-hide-add-active,.ng-hide-remove{display:block!important;}</style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0;">
    <meta name="description" content="Tìm vé tàu, đặt vé tàu trực tuyến, mua vé tàu trực tuyến, tra cứu thông tin hành trình về giờ tàu và giá vé, tra cứu lại thông tin vé đã đặt">
    <meta name="author" content="Tổng công ty đường sắt Việt Nam">
    
    <link rel='stylesheet' media='screen and (min-width: 912px)' href="<?php echo base_url()?>assets/css/bootstrap-cerulean.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/content/ETicket-1.0.29.css" />
    <link rel='stylesheet' media='screen and (max-width: 911px)' href="<?php echo base_url()?>assets/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        @media (min-width: 1286px) {
            .adv-left {
                float: left;
                display: block;
                position: fixed;
                top: 146px;
                left: calc(50% - 629px);
                left: -webkit-calccalc(50% - 629px);
                left: -moz-calc(50% - 629px);
            }
            .adv-right {
                float: right;
                display: block;
                position: fixed;
                top: 146px;
                right: calc(50% - 629px);
                right: -webkit-calccalc(50% - 629px);
                right: -moz-calc(50% - 629px);
            }
        }

        @media (max-width: 1286px) {
            .adv-left {
                display: none;
            }
            .adv-right {
                display: none;
            }
        }
    </style>
    <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/v1525468050349/recaptcha__en.js"></script><script type="text/javascript" async="" src="https://ssl.google-analytics.com/ga.js"></script><script type="text/javascript" async="" src="https://static.subiz.com/public/js/loader.js"></script><script async="" src="//www.google-analytics.com/analytics.js"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-M5KLS6"></script><script type="text/javascript">

        var abp = abp || {};
        abp.appPath = '/';
    </script>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-M5KLS6');</script>
   
    <style>
        .navbar-toggle {
            margin-right: 28px;
        }

        .navbar {
            margin-bottom: 5px;
            margin: -0.5px -15px;
        }

        .navbar-brand {
            padding: 15px 30px;
        }

        .navbar-nav > li > a {
            padding-left: 30px;
            color: white;
        }

        .navbar-default .navbar-brand {
            color: white;
        }

        .navbar-default .navbar-nav > li > a {
            color: #f1f1f1;
            transition: 0.5s;
        }

        .navbar-default .navbar-nav > li:hover {
            background-color: #1995dc;
        }

        .navbar-nav {
            margin: 0.5px -15px;
        }

        .navbar-default .navbar-toggle .icon-bar {
            background-color: #f1f1f1;
        }

        .cart-item {
            position: absolute;
            right: 13px;
            top: 30px;
            background: #f89406;
            color: #ffffff;
            padding: 2px 5px;
            display: inline-block;
            border-radius: 16px;
            font-size: 9px;
            z-index: 1000;
        }

        .fa {
            display: inline-block;
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            line-height: 1;
        }

        .visible-xs {
            display: block !important;
        }
    @-webkit-keyframes marqueeAnimation-327018  { 100%  {margin-left:-2809px}}</style>
</head>
<body class="">
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M5KLS6"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <div ng-app="app" class="super-container ng-scope">
        <div id="menu-fixed">
            <div class="visible-print">
                <h3 style="border-bottom: solid 1px #ccc;padding-bottom: 6px;margin-bottom: -20px;">Tổng công ty Đường sắt Việt Nam</h3>
            </div>
            <div class="et-banner hidden-print">
                <div class="container et-banner" style="max-width:1024px;">
                    <div class="pull-left banner-logo">
                        <div class="pull-left banner-logo"><img src="/images/LOGO_n.png"></div>
                        <div class="pull-left" id="bannerDate">Thứ ba, 08/05/2018</div>
                    </div>
                    <div class="pull-right banner-language">
                        <div class="text-right" ng-show="!bannerTet">
                            <g="vi"><img src="/images/vn-icon.gif" width="20" height="16" /></a>-->
                        </div>
                        <div class="text-right banner-logo-2" style="padding-top: 20px;"><img src="/images/fpt-logo2.png" height="48" width="75"></div>
                        <div class="text-right ticket-cart-number" style="padding-top: 6px; display:none;">
                            <div et-ticket-cart-number="" class="ng-isolate-scope">
<div class="visible-xs">
    <a analytics-on="click" analytics-event="Checkout" href="" ng-click="pocketDetail($event)" id="btnCheckOut" target="_blank" class="btn">
        <span class="cart-item ng-binding">1</span>
        <img src="../../images/Shopping-Cart-03.png" style="font-size:20px;z-index: 1; width:25px;">
    </a>
</div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container et-main-content ng-scope ng-hide" ng-show="bannerTet" ng-include="'app/tet/tet.html'"><div class="shuffle-animation col-md-12 ng-scope">
    <h3 class="text-center" style="margin: 20px; font-size: 32px;color: #d64e00; font-family: 'Times New Roman';" id="tet-text">
        Đường sắt Việt Nam sẽ chính thức mở bán vé tàu Tết Đinh Dậu 2017<br>
        vào lúc 08:00 ngày 1 tháng 10 năm 2017
    </h3>
    <h3 class="text-center" style="margin: 20px; font-size: 18px;color: #0C4DA2;" id="tet-remain">Hệ thống sẽ mở bán trong:</h3>
    <div style="padding-left: 226px;">
        <div id="tet-clockdiv">
            <div>
                <span class="tet-days">00</span>
                <div class="tet-smalltext">Ngày</div>
            </div>
            <div>
                <span class="tet-hours">00</span>
                <div class="tet-smalltext">Giờ</div>
            </div>
            <div>
                <span class="tet-minutes">00</span>
                <div class="tet-smalltext">Phút</div>
            </div>
            <div>
                <span class="tet-seconds">00</span>
                <div class="tet-smalltext">Giây</div>
            </div>
        </div>
    </div>

    <div class="text-center" style="padding: 8px;">
        <img src="/images/banner-tet-2018.jpg" alt="banner-tet-2017.jpg" width="510">
    </div>
</div>
<div class="et-col-md-12 et-footer hidden-print ng-scope">

    <div class="row et-footer-logo-group">

        <div class="et-col-md-12 et-footer-logo">
            <div class="et-col-md-12 text-center" style="font-size: 10px; color: #999;">
                Tổng công ty Đường  sắt Việt Nam. Số 118 Lê Duẩn, Hoàn Kiếm, Hà Nội. Điện thoại: 19006469. Email: dsvn@vr.com.vn
                <br>

                Giấy chứng nhận ĐKKD số 113642 theo QĐ thành lập số 973/QĐ-TTg ngày 25/06/2010 của Thủ tướng Chính phủ.
                <br>

                Mã số doanh nghiệp: 0100105052, đăng ký lần đầu ngày 26/07/2010, đăng ký thay đổi lần 4 ngày 27/06/2014 tại Sở KHĐT Thành phố Hà Nội.
            </div>
        </div>

        <div class="et-col-md-12 text-center" style="font-size: 10px; color: #999;">
            <img src="/images/fptlogo.png" height="17" width="28">
            Copyright by FPT Technology Solutions
        </div>
    </div>
</div></div>
        <div class="navbar-default bg-primary ng-scope" role="navigation" ng-controller="app.controllers.layout.layout" ng-show="!bannerTet" style="margin-top:10PX;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-ex1-collapse" aria-expanded="false" id="buton-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav" id="nav">
                        <li ng-repeat="route in routes | filter:true:route.config.showInMenu" ng-click="changeMenuItem(route.config)" class="ng-scope">
                            <a ng-href="#/" ng-class="{'et-text-menu': !route.config.menuImage, 'et-image-menu': route.config.menuImage}" target="" class="nav-menu et-image-menu" href="#/">
                                <span class="glyphicon glyphicon-home ng-scope" ng-if="route.config.menuImage != undefined"></span>
                            </a>
                        </li><li ng-repeat="route in routes | filter:true:route.config.showInMenu" ng-click="changeMenuItem(route.config)" class="ng-scope">
                            <a ng-href="#/timve" ng-class="{'et-text-menu': !route.config.menuImage, 'et-image-menu': route.config.menuImage}" target="" class="nav-menu et-text-menu" href="#/timve">
                                <span ng-if="route.config.menuImage == undefined" class="ng-binding ng-scope">TÌM VÉ</span>
                            </a>
                        </li><li ng-repeat="route in routes | filter:true:route.config.showInMenu" ng-click="changeMenuItem(route.config)" class="ng-scope">
                            <a ng-href="#/thongtingiaodich" ng-class="{'et-text-menu': !route.config.menuImage, 'et-image-menu': route.config.menuImage}" target="" class="nav-menu et-text-menu" href="#/thongtingiaodich">
                                <span ng-if="route.config.menuImage == undefined" class="ng-binding ng-scope">THÔNG TIN ĐẶT CHỖ</span><
                            </a>
                        </li><li ng-repeat="route in routes | filter:true:route.config.showInMenu" ng-click="changeMenuItem(route.config)" class="ng-scope">
                            <a ng-href="#/kiemtrave" ng-class="{'et-text-menu': !route.config.menuImage, 'et-image-menu': route.config.menuImage}" target="" class="nav-menu et-text-menu" href="#/kiemtrave">
                                <span ng-if="route.config.menuImage == undefined" class="ng-binding ng-scope">KIỂM TRA VÉ</span>
                            </a>
                        </li><li ng-repeat="route in routes | filter:true:route.config.showInMenu" ng-click="changeMenuItem(route.config)" class="ng-scope">
                            <a ng-href="http://k.vnticketonline.vn/#/thongtinhanhtrinh/gadi" ng-class="{'et-text-menu': !route.config.menuImage, 'et-image-menu': route.config.menuImage}" target="_blank_" class="nav-menu et-text-menu" href="http://k.vnticketonline.vn/#/thongtinhanhtrinh/gadi">
                                <span ng-if="route.config.menuImage == undefined" class="ng-binding ng-scope">GIỜ TÀU - GIÁ VÉ</span>
                            </a>
                        </li><li ng-repeat="route in routes | filter:true:route.config.showInMenu" ng-click="changeMenuItem(route.config)" class="ng-scope">
                            <a ng-href="#/khuyenmai" ng-class="{'et-text-menu': !route.config.menuImage, 'et-image-menu': route.config.menuImage}" target="" class="nav-menu et-text-menu" href="#/khuyenmai">
                                <span ng-if="route.config.menuImage == undefined" class="ng-binding ng-scope">KHUYẾN MẠI</span>
                            </a>
                        </li><li ng-repeat="route in routes | filter:true:route.config.showInMenu" ng-click="changeMenuItem(route.config)" class="ng-scope">
                            <a ng-href="#/quydinhmuave" ng-class="{'et-text-menu': !route.config.menuImage, 'et-image-menu': route.config.menuImage}" target="" class="nav-menu et-text-menu" href="#/quydinhmuave">
                                <span ng-if="route.config.menuImage == undefined" class="ng-binding ng-scope">CÁC QUY ĐỊNH</span>
                            </a>
                        </li><li ng-repeat="route in routes | filter:true:route.config.showInMenu" ng-click="changeMenuItem(route.config)" class="ng-scope">
                            <a ng-href="#/huongdan" ng-class="{'et-text-menu': !route.config.menuImage, 'et-image-menu': route.config.menuImage}" target="" class="nav-menu et-text-menu" href="#/huongdan">
                                <span ng-if="route.config.menuImage == undefined" class="ng-binding ng-scope">HƯỚNG DẪN</span>
                            </a>
                        </li><li ng-repeat="route in routes | filter:true:route.config.showInMenu" ng-click="changeMenuItem(route.config)" class="ng-scope">
                            <a ng-href="#/lienhe" ng-class="{'et-text-menu': !route.config.menuImage, 'et-image-menu': route.config.menuImage}" target="" class="nav-menu et-text-menu" href="#/lienhe">
                                <span ng-if="route.config.menuImage == undefined" class="ng-binding ng-scope">LIÊN HỆ</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" style="padding: 4px 9px !important; display: none;" class="nav-menu et-text-menu" translation-lang="vi" ng-style="{'display': language=='vi'?'none':'block'}">TIẾNG VIỆT &nbsp;<img src="/images/vn-icon.gif" height="16" width="20"></a>
                            <a href="javascript:;" style="padding: 4px 9px !important; display: block;" class="nav-menu et-text-menu" translation-lang="en" ng-style="{'display': language=='en'?'none':'block'}">ENGLISH &nbsp;<img src="/images/en-icon.png" height="20" width="20"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="adv-left">
            <a target="_blank" href="http://www.vr.com.vn/cam-nang-di-tau/khuyen-cao-khach-hang-chu-y-khi-mua-ve-truc-tuyen.html">
                <img src="images/dsvn1.jpg">
            </a>
        </div>
        </style>
        <div class="col-md-12" ng-show="!isViewConfirm &amp;&amp; !isFormDichVu"><div class="et-page-header"><span class="et-main-label ng-binding" style="float:left">THÔNG TIN GIỎ VÉ</span>&nbsp;&nbsp; <a class="list-ticket-mobile" ng-click="thongBaoHetThoiGianGiuCho()" style="float:left"><img style="margin-bottom:3PX" src="../../images/icon_info.png" height="32"></a></div><div class="row list-ticket-deskhop"><div class="alert alert-info"><p ng-bind-html="'PBuyTicket_dienGiaiGioVe'|translate" class="ng-binding">Các vé có biểu tượng <img src="/images/waring20.png"> là các vé bị hết thời gian tạm giữ. Xin vui lòng loại bỏ các vé này khỏi danh sách vé đặt mua trước khi thực hiện giao dịch thanh toán tiền.</p><p ng-bind-html="'PBuyTicket_dienGiaiNhapTT'|translate" class="ng-binding">Quý khách vui lòng điền đầy đủ, chính xác tất cả các thông tin về hành khách đi tàu bao gồm: Họ tên đầy đủ, số giấy tờ tùy thân (Số chứng minh nhân dân hoặc số hộ chiếu hoặc số giấy phép lái xe đường bộ được pháp luật Việt Nam công nhận hoặc ngày tháng năm sinh nếu là trẻ em hoặc thẻ sinh viên nếu là sinh viên). Để đảm bảo an toàn, minh bạch trong quá trình bán vé các thông tin này sẽ được nhân viên soát vé kiểm tra trước khi lên tàu theo đúng các quy định của Tổng công ty Đường sắt Việt Nam.</p></div></div><div ng-include="'app/payment/ticketCart.form.desktop.html'" class="ng-scope"><div class="row form-group table-responsive list-ticket-deskhop ng-scope" ng-class="{'et-error-block': maxPassengerError || shortyNameError}">
    <table class="table table-bordered">
        <thead class="et-table-header">
            <tr>
                <th style="background-color: lavender;width: 25%;" class="ng-binding">Họ tên</th>
                <th style="width: 120px;background-color: lavender;" class="ng-binding">Thông tin chỗ</th>
                <th style="background-color: lavender;" class="ng-binding">Giá vé</th>
                <th style="background-color: lavender;" class="ng-binding">Giảm đối tượng</th>
                <th style="background-color: lavender;" class="ng-binding">Khuyến mại</th>
                <th style="background-color: lavender;" class="ng-binding">Bảo hiểm</th>
                <th style="background-color: lavender;width: 120px;" class="ng-binding">Thành tiền (VNĐ)</th>
                <th style="background-color: lavender;width: 25px;"></th>
            </tr>
        </thead>
        <tbody ng-show="ves.khuHoi.length > 0" class="ng-hide">
            <tr class="et-table-group-header" ng-show="ves.chieuDi.length > 0 || ves.chieuVe.length > 0">
                <td colspan="8"><label class="ng-binding">Khứ hồi</label></td>
            </tr>
        </tbody>
        <tbody ng-show="ves.chieuDi.length > 0" class="">
            <tr class="et-table-group-header ng-hide" ng-show="ves.khuHoi.length > 0 || ves.chieuVe.length > 0">
                <td colspan="8"><label class="ng-binding">Chiều đi</label></td>
            </tr>
            <tr ng-repeat="ve in ves.chieuDi" ng-class="{'et-error-tr': ve.buyingRuleError || ve.sameNameError}" class="ng-scope">
                <td class="et-table-cell" style="padding: 0px;border-bottom: solid 2px #ccc;">
                    <div class="input-group input-group-sm" style="margin-bottom: 6px;width: 100%;">
                        <span class="input-group-addon text-left ng-binding" style="width: 84px;">Họ tên</span>
                        <input placeholder="Thông tin hành khách" ng-model="ve.hanhKhach.hoTen" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" ng-class="{'et-error-block': ve.nameMissing}" ng-change="syncRegister($index, false)" style="border-top-right-radius: 4px !important;border-bottom-right-radius: 4px !important;" required="" type="text">
                    </div>
                    <div class="input-group input-group-sm" style="margin-bottom: 6px;width: 100%;">
                        <span class="input-group-addon text-left ng-binding" style="width: 84px;">Đối tượng</span>
                        <select class="form-control input-sm ng-pristine ng-valid ng-valid-required" ng-model="ve.hanhKhach.loaiKhach" ng-change="changeLoaiHanhKhach(ve)" ng-class="{'et-error-block': ve.typeMissing}" ng-options="passengerType.TenPTOnline|translate group by passengerType.Nhom|translate for passengerType in passengerTypes" id="pt67384726" style="border-top-right-radius: 4px !important;border-bottom-right-radius: 4px !important;" required=""><option value="0" selected="selected" label="Người lớn">Người lớn</option><option value="1" label="Trẻ em">Trẻ em</option><option value="2" label="Sinh viên">Sinh viên</option><option value="3" label="Người cao tuổi">Người cao tuổi</option></select>
                    </div>
                    <div class="input-group input-group-sm" style="width: 100%;">
                        <span class="input-group-addon text-left ng-binding" style="width: 84px;">Số giấy tờ</span>
                        <input placeholder="Số CMND/ Hộ chiếu/ Ngày tháng năm sinh trẻ em" ng-model="ve.hanhKhach.soCMND" maxlength="19" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" ng-class="{'et-error-block': ve.identityMissing}" ng-disabled="ve.hanhKhach.loaiKhach &amp;&amp; (ve.hanhKhach.loaiKhach.MaPT == 2 || ve.hanhKhach.loaiKhach.MaPT == 99)" ng-change="syncRegister($index, true)" style="border-top-right-radius: 4px !important;border-bottom-right-radius: 4px !important;" required="" type="text">
                    </div>
                </td>
                <td style="font-size: 10px;border-bottom: solid 2px #ccc;">
                    <div class="text-center ng-hide" ng-show="ve.seat.Status.Status != 6">
                        <img src="/images/waring20.png" tooltip="" class="ng-scope">
                    </div>
                    <div class="text-center text-info" ng-show="ve.seat.Status.Status == 6">
                        <span ng-bind-html="'PBuyTicket_thoiGianTamGiu'|translate:ve.seat.Status.Duration" class="ng-binding">Giữ trong <span class="text-danger">558</span> giây</span>
                    </div>
                    <div>
                        <div class="ng-binding">
                            SE7 Hà Nội-Sài Gòn
                        </div>
                        <div class="ng-binding">
                            17/05/2018 06:00
                        </div>
                        <div class="ng-binding">
                            Toa 1 chỗ 40
                        </div>
                        <div class="ng-binding">
                            Ngồi cứng điều hòa
                        </div>
                    </div>
                </td>
                <td class="et-table-cell text-right ng-binding" style="border-bottom: solid 2px #ccc;">
                    667,000
                </td>
                <td class="et-table-cell text-right ng-binding" style="border-bottom: solid 2px #ccc;">
                    0
                </td>
                <td class="et-table-cell text-left" style="border-bottom: solid 2px #ccc;">
                    <div ng-show="!ve.isloadedKhuyenMai" class="ng-binding ng-hide">
                        Đang tìm khuyến mại...
                    </div>
                    <div ng-show="ve.isloadedKhuyenMai" class="">
                        <div ng-show="ve.ListKhuyenMai.length==0" class="ng-binding">
                            Không có khuyến mại cho vé này
                        </div>
                        <div ng-show="ve.ListKhuyenMai.length>0" class="ng-hide">
                            <div ng-show="ve.ListKhuyenMaiIDApDung.length>0" class="ng-hide">
                                <div>
                                    <div>
                                        <span style="font-size: 12px;" class="ng-binding"></span>
                                        <span style="font-size: 12px;" class="ng-binding">(giảm 0)</span>
                                    </div>
                                    <div ng-show="ve.khuyenMai&amp;&amp;ve.TangGiam!=0" style="font-size: 12px;" class="ng-hide">
                                        <div class="text-danger ng-binding">
                                            * Lưu ý:
                                            <span ng-bind-html="'PBuyTicket_luuYTraVeChiTiet'|translate" class="ng-binding">Đây là vé áp dụng chương trình khuyến mại, đề nghị quý khách đọc kỹ</span>
                                        </div>
                                    </div>
                                </div>
                                <div style="font-size: 12px;">
                                    <a href="javascript:;" ng-click="openSelectKhuyenMai(ve)" class="ng-binding">
                                        Các quy định về điều kiện mua vé, trả vé, đổi vé và hủy vé
                                    </a>
                                </div>
                            </div>
                            <div ng-show="ve.ListKhuyenMaiIDApDung.length==0" class="">
                                <div style="font-size: 12px;" class="ng-binding">Không sử dụng khuyến mại</div>
                                <div style="font-size: 12px;">
                                    <a href="javascript:;" ng-click="openSelectKhuyenMai(ve)" class="ng-binding">
                                        Chọn khuyến mại
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="et-table-cell text-right ng-binding" style="border-bottom: solid 2px #ccc;">
                    1,000
                </td>
                <td class="et-table-cell text-right ng-binding" style="border-bottom: solid 2px #ccc;">
                    668,000
                </td>
                <td style="vertical-align: middle;border-bottom: solid 2px #ccc;">
                    <a analytics-on="click" analytics-event="removeTicket" href="" class="et-btn-cancel" ng-click="removeTicket(ve)" ng-show="!ve.seat.waiting"></a>
                    <img src="/images/loading51.gif" style="width: 30px;" ng-show="ve.seat.waiting" class="ng-hide">
                </td>
            </tr>
        </tbody>
        <tbody ng-show="ves.chieuVe.length > 0" class="ng-hide">
            <tr class="et-table-group-header" ng-show="ves.khuHoi.length > 0 || ves.chieuDi.length > 0">
                <td colspan="8">
                    <label class="ng-binding">Chiều về</label>&nbsp;
                    <span><a class="et-btn ng-binding" id="btnCloneBookingInfo" ng-show="ves.chieuDi.length > 0" ng-click="setBackInfo()">Lấy thông tin từ chiều đi</a></span>
                </td>
            </tr>
        </tbody><tfoot>
            <tr class="info">
                <td colspan="6">
                    <span class="pull-right"><strong class="ng-binding">Tổng tiền</strong></span>
                </td>
                <td class="text-right">
                    <strong class="ng-binding">668,000</strong>
                </td>
                <td>&nbsp;</td>
            </tr>
        </tfoot>
    </table>
</div>
</div><div ng-include="'app/payment/ticketCart.form.mobile.html'" class="ng-scope"><div class="list-ticket-mobile ng-scope">
    <div class="panel panel-info ng-hide" ng-show="ves.khuHoi.length > 0" style="margin-left:-30px; margin-right:-26px;">
        <div class="panel-heading ng-binding">Khứ hồi</div>
        <div class="panel-body">
        </div>
    </div>
    <div class="panel panel-info" ng-show="ves.chieuDi.length > 0" style="margin-left:-30px; margin-right:-26px;">
        <div class="panel-heading ng-binding">Chiều đi</div>
        <div class="panel-body">
            <div class="row ng-scope" ng-repeat="ve in ves.chieuDi">
                <div class="ticket-cart">
                    <div style="color:#175d94;line-height: 25px;font-size:12px;">
                        <div class="col-xs-10 col-sm-10 text-left ng-binding">
                            -  Tàu  <span class="text-info-ticket ng-binding">SE7</span>
                             Toa  <span class="text-info-ticket ng-binding">1 </span>
                             Chỗ  <span class="text-info-ticket ng-binding">40</span>
                             Ngồi cứng điều hòa
                        </div>
                        <div class="col-xs-2 col-sm-2 remove-ticket">
                            <div class="remove-ticket-expired ng-hide" ng-show="ve.seat.Status.Status != 6">
                                <img src="/images/waring20.png" tooltip="" class="ng-scope">
                            </div>
                            <div class="remove-ticket-timing" ng-show="ve.seat.Status.Status == 6">
                                <span class="text-danger ng-binding">558</span>
                            </div>
                            <a class="et-btn-cancel remove-ticket-icon" href="javascript:;" ng-click="removeTicket(ve)" ng-show="!ve.seat.waiting"></a>
                            <img class="remove-ticket-loading ng-hide" src="/images/loading51.gif" style="width: 30px;" ng-show="ve.seat.waiting">
                        </div>
                    </div>
                    <div style="color:#175d94;line-height: 25px;font-size:13px;">
                        <div class="col-xs-11 col-sm-11 text-left ng-binding">
                            -   <span class="text-info-ticket ng-binding"> Hà Nội </span>
                             Đi  <span class="text-info-ticket ng-binding"> Sài Gòn </span>
                             Ngày  <span class="text-info-ticket ng-binding"> 17/05/2018 06:00 </span>
                        </div>
                        <div class="col-xs-1 col-sm-1"></div>
                    </div>
                    <div style="color:#175d94;line-height: 25px; font-size:12px;">
                        <div class="col-xs-12 col-sm-12 text-left">

                            <div ng-show="!ve.isloadedKhuyenMai" class="ng-binding ng-hide">
                                Đang tìm khuyến mại...
                            </div>
                            <div ng-show="ve.isloadedKhuyenMai" class="">
                              
                                <div ng-show="ve.ListKhuyenMai.length>0" class="ng-hide">
                                    <div ng-show="ve.ListKhuyenMaiIDApDung.length>0" class="ng-hide">
                                        <div>
                                            <div class="ng-binding">
                                                -  Khuyến mại : 
                                                <span class="text-info-ticket">
                                                    <span style="font-size: 12px;" class="ng-binding"></span>
                                                    <span style="font-size: 12px;" class="ng-binding">(giảm 0)</span>
                                                </span>
                                            </div>
                                            <div ng-show="ve.khuyenMai&amp;&amp;ve.TangGiam!=0" style="font-size: 12px;" class="ng-hide">
                                                <div class="text-danger ng-binding">
                                                    * Lưu ý:
                                                    <span ng-bind-html="'PBuyTicket_luuYTraVeChiTiet'|translate" class="ng-binding">Đây là vé áp dụng chương trình khuyến mại, đề nghị quý khách đọc kỹ</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="font-size: 12px;">
                                            <a style="text-decoration:underline !important;" href="javascript:;" ng-click="openSelectKhuyenMai(ve)" class="ng-binding">
                                                Các quy định về điều kiện mua vé, trả vé, đổi vé và hủy vé
                                            </a>
                                        </div>
                                    </div>
                                    <div ng-show="ve.ListKhuyenMaiIDApDung.length==0" class="">
                                        <div style="font-size: 12px;" class="ng-binding">Không sử dụng khuyến mại</div>
                                        <div style="font-size: 12px;">
                                            <a href="javascript:;" ng-click="openSelectKhuyenMai(ve)" class="ng-binding">
                                                Chọn khuyến mại
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="color:#175d94;line-height: 25px; font-size:12px;">
                        <div class="col-xs-12 col-sm-12 text-left ng-binding" style="margin-bottom:10px;">
                            -  Thành tiền (VNĐ) :<span class="text-info-ticket ng-binding"> 668,000 </span>  VNĐ
                        </div>
                    </div>
                    <div class="row" style="margin-left:0px; margin-bottom:6px;">
                        <div class="col-xs-3 col-sm-3">
                            <span class="text-left ng-binding" style="border-radius: 3px;font-size:13px;">Họ tên</span>
                        </div>
                        <div class="col-xs-9 col-sm-9">
                            <input ng-model="ve.hanhKhach.hoTen" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" placeholder="Họ tên" ng-class="{'et-error-block': ve.nameMissing}" ng-change="syncRegister($index, false)" style="border-top-right-radius: 4px !important;border-bottom-right-radius: 4px !important;" required="" type="text">
                        </div>
                    </div>
                    <div class="row" style="margin-left:0px; margin-bottom:6px;">
                        <div class="col-xs-3 col-sm-3">
                            <span class="text-left ng-binding" style="border-radius: 3px;font-size:13px;">Đối tượng</span>
                        </div>
                        <div class="col-xs-9 col-sm-9">
                            <select class="form-control input-sm ng-pristine ng-valid ng-valid-required" ng-model="ve.hanhKhach.loaiKhach" ng-change="changeLoaiHanhKhach(ve)" ng-class="{'et-error-block': ve.typeMissing}" ng-options="passengerType.TenPTOnline|translate group by passengerType.Nhom|translate for passengerType in passengerTypes" id="pt67384726" style="border-top-right-radius: 4px !important;border-bottom-right-radius: 4px !important;" required=""><option value="0" selected="selected" label="Người lớn">Người lớn</option><option value="1" label="Trẻ em">Trẻ em</option><option value="2" label="Sinh viên">Sinh viên</option><option value="3" label="Người cao tuổi">Người cao tuổi</option></select>
                        </div>
                    </div>
                    <div class="row" style="margin-left:0px; margin-bottom:6px;">
                        <div class="col-xs-3 col-sm-3">
                            <span class="text-left ng-binding" style="border-radius: 3px;font-size:13px;">Số giấy tờ</span>
                        </div>
                        <div class="col-xs-9 col-sm-9">
                            <input ng-model="ve.hanhKhach.soCMND" maxlength="19" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" ng-class="{'et-error-block': ve.identityMissing}" placeholder="Số giấy tờ" ng-disabled="ve.hanhKhach.loaiKhach &amp;&amp; (ve.hanhKhach.loaiKhach.MaPT == 2 || ve.hanhKhach.loaiKhach.MaPT == 99)" ng-change="syncRegister($index, true)" style="border-top-right-radius: 4px !important;border-bottom-right-radius: 4px !important;" required="" type="text">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-info ng-hide" ng-show="ves.chieuVe.length > 0" style="margin-left:-30px; margin-right:-26px;">
        <div class="panel-heading ng-binding">
            Chiều về<    <span><a class="btn btn-primary ng-binding" id="btnCloneBookingInfo" ng-show="ves.chieuDi.length > 0" ng-click="setBackInfo()">Lấy thông tin từ chiều đi</a></span>
        </div>
        <div class="panel-body">
        </div>
    </div>
</div>
</div>
<div class="et-register-block"><h5 class="ng-binding">Thông tin người đặt vé</h5>
<div class="row text-info ng-binding" ng-bind-html="'PBuyTicket_dienGiaiNguoiDat'|translate">Quý khách vui lòng điền đẩy đủ và chính xác các thông tin về người mua vé dưới đây. Các thông tin này sẽ được sử dụng để xác minh người mua vé và lấy vé tại ga trước khi lên tàu theo đúng các quy định của Tổng công ty Đường sắt Việt Nam.</div>
<div class="form-horizontal form-group"><div class="row ng-hide" ng-show="registerNameError || registerEmailRequiredError || registerEmailValidError"><div class="col-xs-4 et-col-md-2"></div><div class="col-xs-8 et-col-md-4"><span class="et-error-label ng-binding ng-hide" ng-show="registerNameError">Hãy nhập họ tên người đăng kí</span></div><div class="col-xs-4 et-col-md-2"></div><div class="col-xs-8 et-col-md-4"><span class="et-error-label ng-binding ng-hide" ng-show="registerIdentityError">Số CMND/Hộ chiếu không hợp lệ</span></div></div>

<div class="row"><label class="col-xs-4 et-col-md-2 text-left ng-binding">Họ và tên<span style="color: red">*</span></label><div class="col-xs-8 et-col-md-4" style="margin-bottom:5px"><input name="fullName" class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" ng-model="register.hoT  en" placeholder="Họ và tên" required="" ng-class="{'et-error-block': registerNameError}" type="text"></div></div><div class="row ng-hide" ng-show="registerEmailReTypeError || registerIdentityError"><div class="col-xs-4 et-col-md-2"></div>

<div class="col-xs-8 et-col-md-4"><span class="et-error-label ng-binding ng-hide" ng-show="registerNameError"></span></div><div class="col-xs-4 et-col-md-2"></div><div class="col-xs-8 et-col-md-4"><span class="et-error-label ng-binding ng-hide" ng-show="registerIdentityError"></span></div></div>
<div class="row"><label class="col-xs-4 et-col-md-2 text-left ng-binding">Ngày tháng năm sinh</label><div class="col-xs-8 et-col-md-4" style="margin-bottom:5px"><input class="form-control input-sm ng-pristine ng-invalid ng-invalid-required" ng-model="register.hoT  en" placeholder="Ngày tháng năm sinh" required="" ng-class="{'et-error-block': registerNameError}" type="text"></div></div><div class="row ng-hide" ng-show="registerEmailReTypeError || registerIdentityError"><div class="col-xs-4 et-col-md-2"></div>



<div class="col-xs-8 et-col-md-4"><span class="et-error-label ng-binding ng-hide" ng-show="registerEmailRequiredError">Hãy nhập email người đăng kí</span><br><span class="et-error-label ng-binding ng-hide" ng-show="registerEmailValidError">Email không hợp lệ</span></div><div class="col-xs-4 et-col-md-2"></div><div class="col-xs-8 et-col-md-4"><span class="et-error-label ng-binding ng-hide" ng-show="registerEmailReTypeError">Xác nhận email không khớp</span></div></div><div class="row"><label class="col-xs-4 et-col-md-2 text-left ng-binding" style="padding-right: 0">Email</label><div class="col-xs-8 et-col-md-4" style="margin-bottom:5px"><input id="email" name="email" class="form-control input-sm ng-pristine ng-valid ng-valid-email" ng-model="register.email" placeholder="Email" ng-class="{'et-error-block': (registerEmailRequiredError || registerEmailValidError)}" type="email"></div>
    </div><div class="row ng-hide" ng-show="registerPhoneError"><div class="col-xs-4 et-col-md-2"></div><div class="col-xs-8 et-col-md-4"><span class="et-error-label ng-binding ng-hide" ng-show="registerPhoneError">Số di động không hợp lệ</span></div></div><div class="row"><label class="col-xs-4 et-col-md-2 text-left ng-binding">Số di động</label><div class="col-xs-8 et-col-md-4" style="margin-bottom:5px"><input id="phoneNumber" name="phoneNumber" class="form-control input-sm ng-pristine ng-valid" ng-model="register.dienThoai" maxlength="20" placeholder="Số di động" ng-class="{'et-error-block': registerPhoneError}" type="text"></div></div></div><div class="row ng-hide" ng-show="shortyNameError || passengerInputError || maxPassengerError || maxTicketError || maxTicketPerTrainError || passengerInfoError || ticketError"><div class="col-md-12 alert alert-warning"><div class="et-error-label"><span class="glyphicon glyphicon-warning-sign"></span> <b class="ng-binding">Thông tin nhập vào chưa chính xác</b></div><div class="et-error-label ng-binding ng-hide" ng-show="passengerInputError">Hãy điền đầy đủ thông tin hành khách</div><div class="et-error-label ng-binding ng-hide" ng-show="maxPassengerError">Mỗi giao dịch chỉ cho tối đa 5 hành khách</div><div class="et-error-label ng-binding ng-hide" ng-show="maxTicketError">Mỗi hành khách chỉ được sở hữu tối đa 2 vé mỗi giao dịch</div><div class="et-error-label ng-binding ng-hide" ng-show="maxTicketPerTrainError">Mỗi hành khách chỉ được sở hữu 1 vé trên 1 chuyến tàu</div><div class="et-error-label ng-binding ng-hide" ng-show="passengerInfoError">Có hành khách khác tên nhưng trùng số CMTND/ Hộ chiếu?</div><div class="et-error-label ng-binding ng-hide" ng-show="shortyNameError">Bạn cần điền đầy đủ họ và tên giống với CMTND</div></div></div><div class="form-group" ng-class="{'et-error-block': notAgreementError}"><div ng-show="notAgreementError" class="ng-hide"><span class="et-error-label ng-binding">Bạn chưa đồng ý với những quy định mua vé trực tuyến.</span></div><div><input ng-model="agreement" class="ng-pristine ng-valid" type="checkbox"> <span ng-bind-html="'PBuyTicket_xacNhanDongYMuaVeOnline'|translate" class="ng-binding">Tôi đã đọc kỹ và đồng ý tuân thủ tất cả các <a href="/#/quydinhmuave" target="_blank">quy định mua vé trực tuyến</a>, <a href="/#/khuyenmai" target="_blank">các chương trình khuyến mại</a> của Tổng công ty đường sắt Việt Nam và chịu trách nhiệm về tính xác thực của các thông tin trên.</span></div></div></div><div class="col-md-12"><a analytics-on="click" analytics-event="SaveBooking-Step1" href="" id="btnSaveBookInfo" ng-click="validateStep1()" class="pull-right et-btn-rec ng-binding" ng-disabled="requesting">Tiếp theo&gt;&gt;</a> <a href="/#/timkiem/ketqua" class="pull-left et-btn-rec ng-binding" ng-disabled="requesting">&lt;&lt; Quay lại</a></div></div><div class="col-md-12 width-layout ng-hide" ng-show="!isViewConfirm &amp;&amp; isFormDichVu"><div class="row"><div class="row et-page-header"><span class="et-main-label ng-binding" style="margin-left:40px">Chọn dịch vụ đi kèm</span>&nbsp;&nbsp;</div></div><div ng-include="'app/payment/ticketCart.dichvu.desktop.html'" class="ng-scope"><div class="row form-group table-responsive list-ticket-deskhop ng-scope" ng-class="{'et-error-block': maxPassengerError || shortyNameError}">
    <table class="table table-bordered">
        <thead class="et-table-header">
            <tr>
                <th style="vertical-align: middle; background-color: lavender;width: 25%;" class="ng-binding">Họ tên</th>
                <th style="vertical-align: middle; width: 120px;background-color: lavender;" class="ng-binding">Thông tin chỗ</th>
                <th style="vertical-align: middle; background-color: lavender;" class="ng-binding">Tiền vé</th>
                <th style="vertical-align: middle; background-color: lavender;" class="ng-binding">Chọn mua dịch vụ</th>
                <th style="vertical-align: middle; background-color: lavender;" class="ng-binding">Giá dịch vụ</th>
                <th style="vertical-align: middle; background-color: lavender;" class="ng-binding">Giảm giá dịch vụ</th>
                <th style="vertical-align: middle; background-color: lavender;width: 100px;" class="ng-binding">Thành tiền (VNĐ)</th>
            </tr>
        </thead>
        <tbody ng-show="ves.khuHoi.length > 0" class="ng-hide">
            <tr class="et-table-group-header" ng-show="ves.chieuDi.length > 0 || ves.chieuVe.length > 0">
                <td colspan="7"><label class="ng-binding">Khứ hồi</label></td>
            </tr>
        </tbody>
        <tbody ng-show="ves.chieuDi.length > 0" class="">
            <tr class="et-table-group-header ng-hide" ng-show="ves.khuHoi.length > 0 || ves.chieuVe.length > 0">
                <td colspan="7"><label class="ng-binding">Chiều đi</label></td>
            </tr>
            <tr ng-repeat-start="ve in ves.chieuDi" ng-class="{'et-error-tr': ve.buyingRuleError || ve.sameNameError}" class="ng-scope">
                <td rowspan="" class="et-table-cell" style="padding: 6px;border-bottom: solid 2px #ccc;">
                    <div class="text-info ng-binding" style="font-size: 11px;">Họ tên: </div>
                    <div class="text-info ng-binding" style="font-size: 11px;">Số giấy tờ: </div>
                    <div class="text-info ng-binding" style="font-size: 11px;">Đối tượng: Người lớn</div>
                </td>
                <td rowspan="" class="et-table-cell" style="font-size: 10px;border-bottom: solid 2px #ccc;">
                    <div class="text-center ng-hide" ng-show="ve.seat.Status.Status != 6">
                        <img src="/images/waring20.png" tooltip="" class="ng-scope">
                    </div>
                    <div class="text-center text-info" ng-show="ve.seat.Status.Status == 6">
                        <span ng-bind-html="'PBuyTicket_thoiGianTamGiu'|translate:ve.seat.Status.Duration" class="ng-binding">Giữ trong <span class="text-danger">558</span> giây</span>
                    </div>
                    <div>
                        <div class="ng-binding">
                            SE7 Hà Nội-Sài Gòn
                        </div>
                        <div class="ng-binding">
                            17/05/2018 06:00
                        </div>
                        <div class="ng-binding">
                            Toa 1 chỗ 40
                        </div>
                        <div class="ng-binding">
                            Ngồi cứng điều hòa
                        </div>
                    </div>
                </td>
                <td rowspan="" class="et-table-cell text-right ng-binding" style="border-bottom: solid 2px #ccc;">
                    668,000
                </td>
                <td class="et-table-cell text-center" style="border-bottom: solid 2px #ccc;">
                </td>
                <td class="et-table-cell text-right" style="border-bottom: solid 2px #ccc;">
                </td>
                <td class="et-table-cell text-right" style="border-bottom: solid 2px #ccc;">
                </td>

            

                <td rowspan="" class="et-table-cell text-right ng-binding" style="border-bottom: solid 2px #ccc;">
                    668,000
                </td>
            </tr>
        </tbody>
        <tbody ng-show="ves.chieuVe.length > 0" class="ng-hide">
            <tr class="et-table-group-header" ng-show="ves.khuHoi.length > 0 || ves.chieuDi.length > 0">
                <td colspan="7">
                    <label class="ng-binding">Chiều về</label>&nbsp;
                </td>
            </tr>
        </tbody><tfoot>
            <tr class="info">
                <td colspan="6">
                    <span class="pull-right"><strong class="ng-binding">Tổng tiền</strong></span>
                </td>
                <td class="text-right">
                    <strong class="ng-binding">668,000</strong>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
        </div><div ng-include="'app/payment/ticketCart.dichvu.mobile.html'" class="ng-scope"><div class="list-ticket-mobile ng-scope" ng-class="{'et-error-block': maxPassengerError || shortyNameError}">
    <div class="panel panel-info ng-hide" style="margin-left:-15px; margin-right:-15px;" ng-show="ves.khuHoi.length > 0" ng-class="{'et-error-tr': ve.buyingRuleError || ve.sameNameError}">
        <div class="panel-heading ng-binding">Khứ hồi</div>
        <div class="panel-body">
        </div>
    </div>
    <div class="panel panel-info" style="margin-left:-15px; margin-right:-15px;" ng-show="ves.chieuDi.length > 0" ng-class="{'et-error-tr': ve.buyingRuleError || ve.sameNameError}">
        <div class="panel-heading ng-binding">Chiều đi</div>
        <div class="panel-body">
            <div class="item-ticket ng-scope" ng-repeat="ve in ves.chieuDi">
                <div class="item-ticket-header">
                    <span class="text-ticket ng-binding"> Vé số 1</span>
                    <span class="time-duration ng-binding" ng-show="ve.seat.Status.Status == 6">558</span>
                    <div class="time-duration-out ng-hide" ng-show="ve.seat.Status.Status != 6">
                        <img src="/images/waring20.png" tooltip="" class="ng-scope">
                    </div>
                </div>
                <div class="item-ticket-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ng-binding">
                            -  Tàu  <span class="text-info-ticket ng-binding">SE7</span> Toa  <span class="text-info-ticket ng-binding">1 </span>
                             Chỗ  <span class="text-info-ticket ng-binding">40</span> Ngồi cứng điều hòa <br>
                            - <span class="text-info-ticket ng-binding">Hà Nội - Sài Gòn</span> ngày <span class="text-info-ticket ng-binding">17/05/2018   06:00 </span><br>
                            -  <span class="ng-binding">Họ tên:</span> <span class="text-info-ticket ng-binding"> </span><br>
                            - <span class="ng-binding">Số giấy tờ:</span> <span class="text-info-ticket ng-binding"></span> <br>
                            -  <span class="ng-binding">Đối tượng:</span><span class="text-info-ticket ng-binding"> Người lớn </span><br>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ng-binding">
                            - Chọn mua dịch vụ :  <br>
                            <div ng-show="listDichVuByTicket[ve.seat.Status.TicketId].length==1" class="ng-binding ng-hide">
                                <div class="text-center">
                                    <input ng-checked="ve.selectedDichVus[listDichVuByTicket[ve.seat.Status.TicketId][0].DichVuDetailID]" ng-click="setUseDichVu(ve, listDichVuByTicket[ve.seat.Status.TicketId][0].DichVuDetailID)" type="checkbox"> <br>
                                    <span class="text-info-ticket ng-binding"></span>
                                </div>
                                -  Giá (VNĐ):<span class="text-info-ticket ng-binding"></span> VNĐ <br>
                                - Giảm giá DV : <span class="text-info-ticket ng-binding"></span> VNĐ <br>
                            </div>
                            <div ng-show="listDichVuByTicket[ve.seat.Status.TicketId].length>1" class="table-responsive ng-hide">
                                
                                        &lt;<table class="table-bordered" style="width: 100%;">
                                    <tbody><tr>
                                        <td style="padding:5px;"></td><td class="text-center ng-binding" style="padding:5px;">Tên dịch vụ</td>
<td class="text-center ng-binding" style="padding:5px;">Giá dịch vụ</td>
<td class="text-center" style="padding:5px;" {{'pbuyticket_giamgiavu'|translate}}=""></td>
                                    </tr>
                                </tbody></table>
                            </div>
                            - Thành tiền  : <span class="text-info-ticket ng-binding">668,000</span> VNĐ <br>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-info ng-hide" ng-show="ves.chieuVe.length > 0" style="margin-left:-15px; margin-right:-15px;">
        <div class="panel-heading ng-binding">Chiều về</div>
        <div class="panel-body">
        </div>
    </div>
    <div class="text-center" style="font-size:16px;font-weight:bold;color:#1e466d; margin-bottom:25px;">
        <span class="ng-binding">Tổng tiền  : 668,000 VNĐ</span>
    </div>
</div></div><div class="col-md-12"><a analytics-on="click" analytics-event="SaveBooking-Step1" href="" id="btnConfirmDichVu" ng-click="viewConfirm(true)" class="pull-right et-btn-rec ng-binding" ng-disabled="requesting">Tiếp theo&gt;&gt;</a> <a href="javascript:;" class="pull-left et-btn-rec ng-binding" ng-click="nhapLaiThongTin()">&lt;&lt; Quay lại</a></div></div><div class="col-md-12 ng-hide" ng-show="isViewConfirm" style="padding: 0"><h2 style="margin-top: 0px;color: #555555" class="ng-binding">Xác nhận thông tin đặt mua vé tàu</h2><h4 style="color:#dd5600;margin-top: 20px;font-weight:600" class="ng-binding">Thông tin người mua vé</h4><div class="row" style="border-bottom: 1px solid #ccc; padding-bottom: 15px"><div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><div><span class="ng-binding">&nbsp;&nbsp;-  Họ và tên:</span> <span class="text-info-ticket ng-binding"></span></div><div><span class="ng-binding">&nbsp;&nbsp;-  Số CMND/Hộ chiếu:</span><span class="text-info-ticket ng-binding"></span></div><div><span class="ng-binding">&nbsp;&nbsp;-  Số di động:</span> <span class="text-info-ticket ng-binding"></span></div><div><span class="ng-binding">&nbsp;&nbsp;-  Email:</span> <span class="text-info-ticket ng-binding"></span></div></div><div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><div><span class="ng-binding">&nbsp;&nbsp;-  Mã hội viên:</span> <span class="text-info-ticket ng-binding"></span></div><div><span class="ng-binding">&nbsp;&nbsp;-  Đơn vị xuất hóa đơn:</span><span class="text-info-ticket ng-binding"></span></div><div><span class="ng-binding">&nbsp;&nbsp;-  Mã số thuế:</span> <span class="text-info-ticket ng-binding"></span></div><div><span class="ng-binding">&nbsp;&nbsp;-  Địa chỉ:</span><span class="text-info-ticket ng-binding"></span></div><div><span class="ng-binding">&nbsp;&nbsp;-  Phương thức thanh toán:</span> <span class="text-info-ticket ng-binding ng-scope ng-hide" ng-show="congThanhToan==item.MaCongTT" ng-repeat="item in listCongTT">Thanh toán trực tuyến</span><span class="text-info-ticket ng-binding ng-scope ng-hide" ng-show="congThanhToan==item.MaCongTT" ng-repeat="item in listCongTT">Thanh toán trên PAYOO</span><span class="text-info-ticket ng-binding ng-scope" ng-show="congThanhToan==item.MaCongTT" ng-repeat="item in listCongTT">Trả sau</span></div></div></div><h4 style="color: #dd5600;margin-top: 20px;font-weight: 600" class="ng-binding">Thông tin vé mua</h4><div ng-include="'app/payment/ticketCart.confirm.desktop.html'" class="ng-scope"><div class="table table-responsive list-ticket-deskhop ng-scope">
    <table class="table table-bordered tbl-border-black ng-hide" ng-show="isHasDichVu">
        <tbody>
            <tr>
                <th class="ng-binding">STT</th>
                <th class="ng-binding">Thông tin vé mua</th>
                <th class="ng-binding">TG giữ vé</th>
                <th class="ng-binding">Giá (VNĐ)</th>
                <th class="ng-binding">Chọn mua dịch vụ</th>
                <th class="ng-binding">Giá dịch vụ</th>
                <th class="ng-binding">Giảm giá dịch vụ</th>
                <th class="ng-binding">Thành tiền (VNĐ)</th>
            </tr>
        </tbody>
        <tbody ng-show="ves.khuHoi.length > 0" class="ng-hide">
            <tr class="" ng-show="ves.chieuDi.length > 0 || ves.chieuVe.length > 0">
                <td colspan="8"><label class="ng-binding">Khứ hồi</label></td>
            </tr>
        </tbody>
        <tbody ng-show="ves.chieuDi.length > 0" class="">
            <tr class="ng-hide" ng-show="ves.khuHoi.length > 0 || ves.chieuVe.length > 0">
                <td colspan="8"><label class="ng-binding">Chiều đi</label></td>
            </tr>
            <tr ng-repeat-start="ve in ves.chieuDi" class="ng-scope">
                <td rowspan="" class="et-table-cell text-center" style="vertical-align: middle;">
                    <div class="ng-binding">1</div>
                </td>
                <td rowspan="" class="et-table-cell">
                    <div class="row">
                        <div class="col-md-4 ng-binding" style="width: 40%;padding-right: 0;"><span class="ng-binding">Họ tên:</span> </div>
                        <div class="col-md-4 ng-binding" style="width: 28%;padding-right: 0;"><span class="ng-binding">Đối tượng:</span> Người lớn</div>
                        <div class="col-md-4" style="width: 32%;padding-right: 0;">
                            <div ng-hide="ve.hanhKhach.loaiKhach &amp;&amp; (ve.hanhKhach.loaiKhach.MaPT == 2 || ve.hanhKhach.loaiKhach.MaPT == 99)" class="ng-binding">
                                <span class="ng-binding">Số giấy tờ:</span> 
                            </div>
                        </div>
                    </div>
                    <div class="ng-binding">
                        <span class="ng-binding">Hành trình:</span>
                        SE7 Hà Nội - Sài Gòn 17/05/2018 06:00
                        Toa 1 chỗ 40 Ngồi cứng điều hòa
                    </div>
                </td>
                <td rowspan="" class="et-table-cell">
                    <div class="text-center ng-binding ng-hide" ng-show="ve.seat.Status.Status != 6">
                        <img src="/images/waring20.png" tooltip="" class="ng-scope">
                        Hết TG giữ
                    </div>
                    <div class="text-center text-info" ng-show="ve.seat.Status.Status == 6">
                        <span ng-bind-html="'PBuyTicket_thoiGianGiuConLai'|translate:ve.seat.Status.Duration" class="ng-binding">Còn <span class="text-danger">558</span> giây</span>
                    </div>
                </td>
                <td rowspan="" class="et-table-cell text-right ng-binding">
                    668,000
                </td>
                <td class="et-table-cell text-center" style="border-bottom: solid 2px #ccc;">
                </td>
                <td class="et-table-cell text-right" style="border-bottom: solid 2px #ccc;">
                </td>
                <td class="et-table-cell text-right" style="border-bottom: solid 2px #ccc;">
                </td>
                
                <td rowspan="" class="et-table-cell text-right ng-binding">
                    668,000
                </td>
            </tr>
        </tbody>
        <tbody ng-show="ves.chieuVe.length > 0" class="ng-hide">
            <tr class="" ng-show="ves.khuHoi.length > 0 || ves.chieuDi.length > 0">
                <td colspan="8">
                    <label class="ng-binding">Chiều về</label>&nbsp;
                </td>
            </tr>
        </tbody><tfoot>
            <tr>
                <td colspan="3">
                    <span class="pull-right"><strong class="ng-binding">Tổng tiền</strong></span>
                </td>
                <td colspan="5" class="text-right">
                    <strong class="ng-binding">668,000 VNĐ</strong>
                </td>
            </tr>
        </tfoot>
    </table>
    <table class="table table-bordered tbl-border-black" ng-show="!isHasDichVu">
        <tbody>
            <tr>
                <th class="ng-binding">STT></th>
                <th class="ng-binding">Thông tin vé mua</th>
                <th class="ng-binding">TG giữ vé</th>
                <th class="ng-binding">Giá (VNĐ)</th>
                <th class="ng-binding">Thành tiền (VNĐ)</th>
            </tr>
        </tbody>
        <tbody ng-show="ves.khuHoi.length > 0" class="ng-hide">
            <tr class="" ng-show="ves.chieuDi.length > 0 || ves.chieuVe.length > 0">
                <td colspan="5"><label class="ng-binding">Khứ hồi</label></td>
            </tr>
        </tbody>
        <tbody ng-show="ves.chieuDi.length > 0" class="">
            <tr class="ng-hide" ng-show="ves.khuHoi.length > 0 || ves.chieuVe.length > 0">
                <td colspan="5"><label class="ng-binding">Chiều đi</label></td>
            </tr>
            <tr ng-repeat="ve in ves.chieuDi" class="ng-scope">
                <td class="et-table-cell text-center">
                    <div class="ng-binding">1</div>
                </td>
                <td class="et-table-cell">
                    <div class="row">
                        <div class="col-md-4 ng-binding" style="width: 40%;padding-right: 0;"><span class="ng-binding">Họ tên:</span> </div>
                        <div class="col-md-4 ng-binding" style="width: 28%;padding-right: 0;"><span class="ng-binding">Đối tượng:</span> Người lớn</div>
                        <div class="col-md-4" style="width: 32%;padding-right: 0;">
                            <div ng-hide="ve.hanhKhach.loaiKhach &amp;&amp; (ve.hanhKhach.loaiKhach.MaPT == 2 || ve.hanhKhach.loaiKhach.MaPT == 99)" class="ng-binding">
                                <span class="ng-binding">Số giấy tờ:</span> 
                            </div>
                        </div>
                    </div>
                    <div class="ng-binding">
                        <span class="ng-binding">Hành trình:</span>
                        SE7 Hà Nội - Sài Gòn 17/05/2018 06:00
                        Toa 1 chỗ 40 Ngồi cứng điều hòa
                    </div>
                </td>
                <td class="et-table-cell">
                    <div class="text-center ng-binding ng-hide" ng-show="ve.seat.Status.Status != 6">
                        <img src="/images/waring20.png" tooltip="" class="ng-scope">
                        Hết TG giữ
                    </div>
                    <div class="text-center text-info" ng-show="ve.seat.Status.Status == 6">
                        <span ng-bind-html="'PBuyTicket_thoiGianGiuConLai'|translate:ve.seat.Status.Duration" class="ng-binding">Còn <span class="text-danger">558</span> giây</span>
                    </div>
                </td>
                <td class="et-table-cell text-right ng-binding">
                    667,000
                </td>
                <td class="et-table-cell text-right ng-binding">
                    668,000
                </td>
            </tr>
        </tbody>
        <tbody ng-show="ves.chieuVe.length > 0" class="ng-hide">
            <tr class="" ng-show="ves.khuHoi.length > 0 || ves.chieuDi.length > 0">
                <td colspan="5">
                    <label class="ng-binding">Chiều về</label>&nbsp;
                </td>
            </tr>
        </tbody><tfoot>
            <tr>
                <td colspan="3">
                    <span class="pull-right"><strong class="ng-binding">Tổng tiền<</strong></span>
                </td>
                <td colspan="2" class="text-right">
                    <strong class="ng-binding">668,000</strong>
                </td>
            </tr>
        </tfoot>
    </table>
</div></div><div ng-include="'app/payment/ticketCart.confirm.mobile.html'" class="ng-scope"><div class="list-ticket-mobile ng-scope">
    <div ng-show="isHasDichVu" class="row ng-hide">
        <div class="panel panel-info ng-hide" ng-show="ves.khuHoi.length > 0">
            <div class="panel-heading ng-binding">Khứ hồi</div>
            <div class="panel-body">
            </div>
        </div>
        <div class="panel panel-info" ng-show="ves.chieuDi.length > 0">
            <div class="panel-heading ng-binding">Chiều đi</div>
            <div class="panel-body">
                <div class="item-ticket ng-scope" ng-repeat="ve in ves.chieuDi">
                    <div class="item-ticket-header">
                        <span class="text-ticket ng-binding"> Vé số 1</span>
                        <span class="time-duration ng-binding" ng-show="ve.seat.Status.Status == 6">558</span>
                        <div class="time-duration-out ng-hide" ng-show="ve.seat.Status.Status != 6">
                            <img src="/images/waring20.png" tooltip="" class="ng-scope">
                        </div>
                    </div>
                    <div class="item-ticket-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ng-binding">
                                -  Tàu  <span class="text-info-ticket ng-binding">SE7</span> Toa  <span class="text-info-ticket ng-binding">1 </span>
                                 Chỗ  <span class="text-info-ticket ng-binding">40</span> Ngồi cứng điều hòa <br>
                                - <span class="text-info-ticket ng-binding">Hà Nội - Sài Gòn</span> ngày <span class="text-info-ticket ng-binding">17/05/2018   06:00 </span><br>
                                -  <span class="ng-binding">Họ tên:</span> <span class="text-info-ticket ng-binding"> </span><br>
                                - <span class="ng-binding">Số giấy tờ:</span> <span class="text-info-ticket ng-binding"></span> <br>
                                -  <span class="ng-binding">Đối tượng:</span><span class="text-info-ticket ng-binding"> Người lớn </span><br>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ng-binding">
                                - Mua dịch vụ :  <span class="text-info-ticket ng-binding"></span><br>
                                - Giá (VNĐ):<span class="text-info-ticket ng-binding"></span> VNĐ <br>
                                - Giảm giá DV : <span class="text-info-ticket ng-binding"></span> VNĐ <br>
                                - Thành tiền (VNĐ):<span class="text-info-ticket ng-binding">668,000</span> VNĐ <br>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-info ng-hide" ng-show="ves.chieuVe.length > 0">
            <div class="panel-heading ng-binding">Chiều về</div>
            <div class="panel-body">
            </div>
        </div>
        <div class="text-center" style="font-size:16px;font-weight:bold;color:#1e466d;">
            <span class="ng-binding">Tổng tiền  : 668,000 VNĐ</span>
        </div>
    </div>
    <div ng-show="!isHasDichVu" class="row">
        <div class="panel panel-info ng-hide" ng-show="ves.khuHoi.length > 0">
            <div class="panel-heading ng-binding">Khứ hồi</div>
            <div class="panel-body">
            </div>
        </div>
        <div class="panel panel-info" ng-show="ves.chieuDi.length > 0">
            <div class="panel-heading ng-binding">Chiều đi</div>
            <div class="panel-body">
                <div class="item-ticket ng-scope" ng-repeat="ve in ves.chieuDi">
                    <div class="item-ticket-header">
                        <span class="text-ticket ng-binding"> Vé số 1</span>
                        <span class="time-duration ng-binding" ng-show="ve.seat.Status.Status == 6">558</span>
                        <div class="time-duration-out ng-hide" ng-show="ve.seat.Status.Status != 6">
                            <img src="/images/waring20.png" tooltip="" class="ng-scope">
                        </div>
                    </div>
                    <div class="item-ticket-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ng-binding">
                                -  <span class="ng-binding">Họ tên:</span> <span class="text-info-ticket ng-binding"> </span><br>
                                - <span class="ng-binding">Số giấy tờ:</span> <span class="text-info-ticket ng-binding"></span> <br>
                                -  <span class="ng-binding">Đối tượng:</span><span class="text-info-ticket ng-binding"> Người lớn </span><br>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ng-binding">
                                -  Tàu  <span class="text-info-ticket ng-binding">SE7</span> Toa  <span class="text-info-ticket ng-binding">1 </span>
                                 Chỗ  <span class="text-info-ticket ng-binding">40</span> Ngồi cứng điều hòa <br>
                                - <span class="text-info-ticket ng-binding">Hà Nội - Sài Gòn</span> ngày <span class="text-info-ticket ng-binding">17/05/2018   06:00 </span><br>
                                -  Thành tiền (VNĐ):<span class="text-info-ticket ng-binding">668,000</span> VNĐ <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-info ng-hide" ng-show="ves.chieuVe.length > 0">
            <div class="panel-heading ng-binding">Chiều về<</div>
            <div class="panel-body">
            </div>
        </div>
        <div class="text-center" style="font-size:16px;font-weight:bold; color:#1e466d; margin-bottom:10px;">
            <span class="ng-binding">Tổng tiền  : 668,000 VNĐ</span>
        </div>
    </div>
</div></div><p ng-bind-html="'PBuyTicket_dienGiaiXacNhanDatVe'|translate" class="ng-binding"><p>Quý khách vui lòng kiểm tra kỹ và xác nhận các thông tin đã nhập trước khi thực hiện giao dịch mua vé.                                              Sau khi thực hiện giao dịch thanh toán ở trang tiếp theo quý khách sẽ không thể thay đổi được thông tin mua vé trên.</p>                                              </p><div class="col-md-12"><a analytics-on="click" analytics-event="SaveBooking" href="javascript:;" id="btnSaveBookInfo" ng-click="validateDataViewConfirm()" class="pull-right et-btn-rec ng-binding" ng-disabled="requesting">Đồng ý xác nhận&gt;&gt;</a> <a href="javascript:;" analytics-on="click" analytics-event="BackToAddInfo" ng-click="nhapLaiThongTin()" class="pull-left et-btn-rec ng-binding" ng-disabled="requesting">&lt;&lt; Nhập lại</a></div></div><div id="childVerifyPopover" class="popover top"><div class="popover-title"><label class="ng-binding">Ngày tháng năm sinh</label></div><div class="popover-content"><p ng-show="!child" ng-bind-html="'PQueuePopover_dienGiaiNguoiCaoTuoi'|translate" class="ng-binding">Người cao tuổi (người từ 60 tuổi trở lên) được hưởng chính sách giảm giá theo quy định của Tổng công ty Đường sắt Việt Nam.</p><p ng-show="child" ng-bind-html="'PQueuePopover_dienGiaiTreEm'|translate" class="ng-binding ng-hide">Trẻ em dưới 6 tuổi không cần phải mua vé<br>Trẻ em từ 6 tuổi đến 10 tuổi được mua vé trẻ em</p><div class="form-group"><select ng-model="date" ng-init="date = 1" class="ng-pristine ng-valid"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29" ng-show="month != 2 || ((year - 2004) % 4) == 0" class="">29</option><option value="30" ng-show="month != 2" class="">30</option><option value="31" ng-show="month == 1 || month == 3 || month == 5 ||month == 7 || month == 8 || month == 10 || month == 12" class="">31</option></select>&nbsp;-<select ng-model="month" ng-options="month for month in months" ng-init="month = months[0]" class="ng-pristine ng-valid"><option value="0" selected="selected" label="1">1</option><option value="1" label="2">2</option><option value="2" label="3">3</option><option value="3" label="4">4</option><option value="4" label="5">5</option><option value="5" label="6">6</option><option value="6" label="7">7</option><option value="7" label="8">8</option><option value="8" label="9">9</option><option value="9" label="10">10</option><option value="10" label="11">11</option><option value="11" label="12">12</option></select>&nbsp;-<select ng-model="year" ng-options="year for year in years" ng-init="year = years[0]" class="ng-pristine ng-valid"><option value="0" selected="selected" label="2018">2018</option><option value="1" label="2017">2017</option><option value="2" label="2016">2016</option><option value="3" label="2015">2015</option><option value="4" label="2014">2014</option><option value="5" label="2013">2013</option><option value="6" label="2012">2012</option><option value="7" label="2011">2011</option><option value="8" label="2010">2010</option><option value="9" label="2009">2009</option><option value="10" label="2008">2008</option><option value="11" label="2007">2007</option><option value="12" label="2006">2006</option><option value="13" label="2005">2005</option><option value="14" label="2004">2004</option><option value="15" label="2003">2003</option><option value="16" label="2002">2002</option><option value="17" label="2001">2001</option><option value="18" label="2000">2000</option><option value="19" label="1999">1999</option><option value="20" label="1998">1998</option><option value="21" label="1997">1997</option><option value="22" label="1996">1996</option><option value="23" label="1995">1995</option><option value="24" label="1994">1994</option><option value="25" label="1993">1993</option><option value="26" label="1992">1992</option><option value="27" label="1991">1991</option><option value="28" label="1990">1990</option><option value="29" label="1989">1989</option><option value="30" label="1988">1988</option><option value="31" label="1987">1987</option><option value="32" label="1986">1986</option><option value="33" label="1985">1985</option><option value="34" label="1984">1984</option><option value="35" label="1983">1983</option><option value="36" label="1982">1982</option><option value="37" label="1981">1981</option><option value="38" label="1980">1980</option><option value="39" label="1979">1979</option><option value="40" label="1978">1978</option><option value="41" label="1977">1977</option><option value="42" label="1976">1976</option><option value="43" label="1975">1975</option><option value="44" label="1974">1974</option><option value="45" label="1973">1973</option><option value="46" label="1972">1972</option><option value="47" label="1971">1971</option><option value="48" label="1970">1970</option><option value="49" label="1969">1969</option><option value="50" label="1968">1968</option><option value="51" label="1967">1967</option><option value="52" label="1966">1966</option><option value="53" label="1965">1965</option><option value="54" label="1964">1964</option><option value="55" label="1963">1963</option><option value="56" label="1962">1962</option><option value="57" label="1961">1961</option><option value="58" label="1960">1960</option><option value="59" label="1959">1959</option><option value="60" label="1958">1958</option><option value="61" label="1957">1957</option><option value="62" label="1956">1956</option><option value="63" label="1955">1955</option><option value="64" label="1954">1954</option><option value="65" label="1953">1953</option><option value="66" label="1952">1952</option><option value="67" label="1951">1951</option><option value="68" label="1950">1950</option><option value="69" label="1949">1949</option><option value="70" label="1948">1948</option><option value="71" label="1947">1947</option><option value="72" label="1946">1946</option><option value="73" label="1945">1945</option><option value="74" label="1944">1944</option><option value="75" label="1943">1943</option><option value="76" label="1942">1942</option><option value="77" label="1941">1941</option><option value="78" label="1940">1940</option><option value="79" label="1939">1939</option><option value="80" label="1938">1938</option><option value="81" label="1937">1937</option><option value="82" label="1936">1936</option><option value="83" label="1935">1935</option><option value="84" label="1934">1934</option><option value="85" label="1933">1933</option><option value="86" label="1932">1932</option><option value="87" label="1931">1931</option><option value="88" label="1930">1930</option><option value="89" label="1929">1929</option><option value="90" label="1928">1928</option><option value="91" label="1927">1927</option><option value="92" label="1926">1926</option><option value="93" label="1925">1925</option><option value="94" label="1924">1924</option><option value="95" label="1923">1923</option><option value="96" label="1922">1922</option><option value="97" label="1921">1921</option><option value="98" label="1920">1920</option><option value="99" label="1919">1919</option></select></div><div class="col-md-12 et-no-padding form-group"><a class="et-btn pull-left text-center ng-binding" ng-click="closeAndClearChildVerify()">Hủy</a> <a class="et-btn pull-right ng-binding" ng-click="confirmChildVerify()">Xác nhận</a></div></div></div></div></div>
            
            <div class="et-col-md-12 et-footer hidden-print ng-scope" ng-controller="app.controllers.layout.footer">
                <div class="et-footer-menu text-center">
                    <a href="/#/" ng-bind-html="'Menu_search'|translate" class="ng-binding">Tìm vé</a>&nbsp;|&nbsp;
                    
                    <a href="/#/thongtingiaodich" ng-bind-html="'Menu_searchBookingInfo'|translate" class="ng-binding">Thông tin đặt chỗ</a>&nbsp;|&nbsp;
                    <a href="http://k.vnticketonline.vn/#/thongtinhanhtrinh/gadi" target="_blank" ng-bind-html="'Menu_trainTimeTable'|translate" class="ng-binding">Giờ tàu - Giá vé</a>&nbsp;|&nbsp;
                    <a href="/#/huongdan" ng-bind-html="'Menu_guide'|translate" class="ng-binding">Hướng dẫn</a>&nbsp;|&nbsp;
                    <a href="/#/lienhe" ng-bind-html="'Menu_contact'|translate" class="ng-binding">Liên hệ</a>
                </div>
                <div class="row" style="margin-top:20px;">

                    <div class="et-col-md-12 et-footer-logo">
                        <div class="et-col-md-12 text-center" style="font-size: 10px; color: #999;">
                            <div ng-bind-html="'Footer_line1'|translate" class="ng-binding">Tổng công ty Đường  sắt Việt Nam. Số 118 Lê Duẩn, Hoàn Kiếm, Hà Nội. Điện thoại: 19006469. Email: dsvn@vr.com.vn.</div>
                            <div ng-bind-html="'Footer_line2'|translate" class="ng-binding">Giấy chứng nhận ĐKKD số 113642 theo QĐ thành lập số 973/QĐ-TTg ngày 25/06/2010 của Thủ tướng Chính phủ.</div>
                            <div ng-bind-html="'Footer_line3'|translate" class="ng-binding">Mã số doanh nghiệp: 0100105052, đăng ký lần đầu ngày 26/07/2010, đăng ký thay đổi lần 4 ngày 27/06/2014 tại Sở KHĐT Thành phố Hà Nội.</div>
                        </div>
                    </div>

                    <div class="et-col-md-12 text-center" style="font-size: 10px; margin-top:15px; color: #999;">
                        <img src="/images/fptlogo.png" height="17" width="28">
                        Copyright by FPT Technology Solutions
                    </div>
                </div>
            </div>
        </div>
        <div class="adv-right">
            <a target="_blank" href="http://www.vr.com.vn/">
                <img src="images/dsvn2.jpg">
            </a>
        </div>
    </div>

    <script type="text/javascript" src="ETicket-1.0.29.js.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-7314325-29', 'auto');

    </script>


    
    <script type="text/javascript">
        function get_browser_info() {
            var e, r = navigator.userAgent, i = r.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            return /trident/i.test(i[1]) ? (e = /\brv[ :]+(\d+)/g.exec(r) || [], { name: "IE", version: e[1] || "" }) : "Chrome" === i[1] && (e = r.match(/\bOPR\/(\d+)/), null != e) ? { name: "Opera", version: e[1] } : (i = i[2] ? [i[1], i[2]] : [navigator.appName, navigator.appVersion, "-?"], null != (e = r.match(/version\/(\d+)/i)) && i.splice(1, 1, e[1]), { name: i[0], version: i[1] })
        }

        function is_mobile() {
            var e = ["android", "webos", "iphone", "ipad", "blackberry"];
            for (i in e) if (navigator.userAgent.match("/" + e[i] + "/i")) return !0;
            return !1
        }

        jQuery(document).ready(function () {
            if (is_mobile()) jQuery("#menu-print").hide();
            else {
                var e = get_browser_info();
                "Chrome" == e.name && screen.width == window.innerWidth && screen.height == window.innerHeight ? (jQuery("#menu-print").show(), jQuery("#invehref").attr("href", KioskPrintUrl)) : jQuery("#menu-print").hide()
            }
        });
    </script>
    <script>
        jQuery(document).ready(function () {
            jQuery('.nav-menu').click(function () {
                jQuery("#bs-example-navbar-collapse-1").removeClass("show");
            });

        });
    </script>

</div></div></body></html>